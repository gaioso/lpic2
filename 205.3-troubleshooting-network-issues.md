# 205.3 - Solucionar problemas de red

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Introducción a la solución de problemas de red (troubleshooting)](#introducción-a-la-solución-de-problemas-de-red-troubleshooting)
    - [Un ejemplo de situación](#un-ejemplo-de-situación)
    - [Problemas de resolución de nombres](#problemas-de-resolución-de-nombres)
    - [Inicialización incorrecta del sistema](#inicialización-incorrecta-del-sistema)
    - [Configuración de seguridad](#configuración-de-seguridad)
    - [Configuración de la red](#configuración-de-la-red)
    - [NetworkManager (Gestor de redes)](#networkmanager-gestor-de-redes)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introducción a la solución de problemas de red (troubleshooting)

Sería estupendo que pudiese abrir un libro y encontrar allí un índice de todos los posibles problemas de red y sus soluciones. Pero en la práctica eso es imposible. Hay demasiados escenarios que describir y las nuevas tecnologías están apareciendo constantemente, creando nuevas opciones y nuevos problemas. Sin embargo, casi todos los problemas se pueden resolver aplicando el conocimiento y la lógica. En el caso de la solución de problemas de red, esto significa principalmente que debe determinar cómo debe fluir el tráfico desde el origen al destino y viceversa, y comprobar paso a paso si puede ver el tráfico que pasa utilizando las herramientas descritas anteriormente. Además, asegúrese de comprobar el tráfico en ambas direcciones si es posible, ya que muchos problemas de red se derivan del hecho de que el tráfico de origen a destino puede no estar siguiendo la misma ruta que el tráfico en sentido contrario.

Los ficheros clave, términos y utilidades ya han sido descritos en el Capítulo 5, *Configuración de redes (205)* y en otros capítulos. En este capítulo nos centramos en el proceso de resolución de problemas e introducimos una serie de técnicas adicionales, utilidades y ficheros clave.

### Un ejemplo de situación

Está navegando por Internet en un PC. Está conectado a Internet a través de una red de área local y un *firewall*. De repente, ya no puede acceder a su página web favorita. Podría ser la red, el firewall, el ISP, o incluso el navegador... ¿cuál es un enfoque razonable para encontrar el problema y resolverlo?

> Nota: Esta sección se centra en la resolución de problemas de redes. Por lo tanto, nos centraremos en los componentes de la red. En una situación real hay muchos más componentes involucrados, por ejemplo, el navegador, el sistema operativo, la configuración del cortafuegos local, etc., sobre los que sólo tocaremos brevemente.

El primer paso es reunir una lista de todos los componentes de red implicados. La longitud de la lista varía, dependiendo de la complejidad de la configuración y de sus conocimientos personales. Una simple lista al menos contendría esto:

* El propio PC. Tiene una interfaz de red que está conectada a la LAN (`eth0` en la mayoría de las situaciones);
* El firewall. Tiene dos interfaces: su interfaz `eth0`, que está conectada a la LAN, y la interfaz `eth1`, que está conectada al router, que a su vez está conectado a un ISP que proporciona conectividad a Internet;
* El sitio al que intenta acceder, conectado a Internet.

Luego piense en cómo funciona todo en conjunto. Introduzca la URL en el navegador. Su máquina utiliza DNS para averiguar cuál es la dirección IP del sitio web al que intenta acceder, etc.

Los paquetes viajan a través de su interfaz `eth0` a través de la LAN a la interfaz `eth0` del firewall y a través de la interfaz `eth1` del firewall al ISP y del ISP de alguna manera al servidor web.

Ahora que sabe cómo interactúan los diferentes componentes, puede tomar medidas para determinar el origen de la avería.

El siguiente gráfico muestra un ejemplo de resolución de problemas paso a paso.

![image](/images/205-7.png)

* S - La causa del problema ha sido determinada y puede ser S(olved)(resuelta).
* 1 - ¿Podemos llegar a otras máquinas en Internet? Pruebe otra URL o intente hacer `ping` a otra máquina en Internet. Tenga cuidado de saltar a las conclusiones si su `ping` informa de que no hay conectividad - su cortafuegos podría estar bloqueando las solicitudes de eco ICMP y las respuestas.
* 2 - ¿La máquina a la que intentamos llegar, el objetivo, ha caído? Esta es una teoría factible si, por ejemplo, se puede llegar a otros sitios. Intente llegar a la máquina a través de otra red, póngase en contacto con un amigo y deje que intente llegar a la máquina, llame a la persona responsable de la máquina, etc.
* 3 - ¿Podemos alcanzar al firewall? Intente hacer `ping` al firewall, inicie sesión en él, etc.
* 4 - ¿Hay un router en la ruta (un "salto") caido? Utilice `traceroute` para averiguar cuáles son los saltos entre usted y el host destino. La ruta de una máquina al servidor web de LPI, por ejemplo, puede determinarse mediante el comando `traceroute -I www.lpi.org`.
	
```
  # traceroute -I www.lpi.org
  traceroute to www.lpi.org (209.167.177.93), 30 hops max, 38 byte packets
  1  fertuut.snowgroningen (192.168.2.1)  0.555 ms  0.480 ms  0.387 ms
  2  wc-1.r-195-85-156.essentkabel.com (195.85.156.1)  30.910 ms  26.352 ms  19.406 ms
  3  HgvL-WebConHgv.castel.nl (195.85.153.145)  19.296 ms  28.656 ms  29.204 ms
  4  S-AMS-IxHgv.castel.nl (195.85.155.2)  172.813 ms  199.017 ms  95.894 ms
  5  f04-08.ams-icr-03.carrier1.net (212.4.194.13)  118.879 ms  84.262 ms  130.855 ms
  6  g02-00.amd-bbr-01.carrier1.net (212.4.211.197)  30.790 ms  45.073 ms  28.631 ms
  7  p08-00.lon-bbr-02.carrier1.net (212.4.193.165)  178.978 ms  211.696 ms  301.321 ms
  8  p13-02.nyc-bbr-01.carrier1.net (212.4.200.89)  189.606 ms  413.708 ms  194.794 ms
  9  g01-00.nyc-pni-02.carrier1.net (212.4.193.198)  134.624 ms  182.647 ms  411.876 ms
  10  500.POS2-1.GW14.NYC4.ALTER.NET (157.130.94.249)  199.503 ms  139.083 ms  158.804 ms
  11  578.ATM3-0.XR2.NYC4.ALTER.NET (152.63.26.242)  122.309 ms  191.783 ms  297.066 ms
  12  188.at-1-0-0.XR2.NYC8.ALTER.NET (152.63.18.90)  212.805 ms  193.841 ms  94.278 ms
  13  0.so-2-2-0.XL2.NYC8.ALTER.NET (152.63.19.33)  131.535 ms  131.768 ms  152.717 ms
  14  0.so-2-0-0.TL2.NYC8.ALTER.NET (152.63.0.185)  198.645 ms  136.199 ms  274.059 ms
  15  0.so-3-0-0.TL2.TOR2.ALTER.NET (152.63.2.86)  232.886 ms  188.511 ms  166.256 ms
  16  POS1-0.XR2.TOR2.ALTER.NET (152.63.2.78)  153.015 ms  157.076 ms  150.759 ms
  17  POS7-0.GW4.TOR2.ALTER.NET (152.63.131.141)  143.956 ms  146.313 ms  141.405 ms
  18  akainn-gw.customer.alter.net (209.167.167.118)  384.687 ms  310.406 ms  302.744 ms
  19  new.lpi.org (209.167.177.93)  348.981 ms  356.486 ms  328.069 ms
```
* 5 - ¿Pueden otras máquinas de la red alcanzar el firewall? Utilice `ping`, o inicie sesión en el firewall desde esa máquina o intente ver una página web en Internet desde esa máquina.
* 6 - ¿El firewall bloquea el tráfico a esa máquina en particular? Tal vez alguien bloqueó el tráfico hacia y/o desde ese sitio.
* 7 - Inspeccione el firewall. Si el problema parece estar en el firewall, pruebe las interfaces en el firewall, inspeccione las reglas del firewall, compruebe el cableado, etc.
* 8 - ¿Está nuestra interfaz `eth0` lista? Esto se puede probar con el comando `ifconfig eth0`.
* 9 - ¿Son sus definiciones de ruta como deberían ser? Piensa en cosas como la puerta de enlace por defecto. La tabla de rutas se puede ver con el comando `route -n`.
* 10 - ¿Hay una razón física para el problema? Compruebe si el problema está en el cableado. Puede tratarse de un cable defectuoso o mal apantallado. Colocar el cableado de la fuente de alimentación y el cableado de datos a través del mismo tubo sin blindaje metálico entre ambos puede causar errores impredecibles y difíciles de reproducir en la transmisión de datos.

### Problemas de resolución de nombres

Hay aún más opciones de por qué falla la conexión:

La resolución de nombres es la traducción de un nombre de host en una dirección IP. Si un usuario intenta conectarse a una máquina basándose en el nombre de host de esa máquina y la resolución del nombre de host no funciona correctamente, no se establecerá ninguna conexión.

El fichero `/etc/resolv.conf` contiene las direcciones IP de los *nameservers*. Los *nameservers* son los servidores que hacen la resolución de nombres para una red externa. Para redes pequeñas (locales) se puede hacer una tabla de búsqueda local (local lookup table) usando el fichero `/etc/hosts`. Este fichero contiene una lista de alias o FQDN (fully qualified domain name)(nombre de dominio completo) (o ambos) por dirección IP.

Puede comprobar la resolución del nombre con los comandos `/usr/bin/dig` (`dig` es un acrónimo de Domain Information Groper) o `/usr/bin/host`. Ambos comandos devuelven la dirección IP asociada al *hostname*.

	$ host ns12.zoneedit.com
	ns12.zoneedit.com has address 209.62.64.46
			
	$ dig zonetransfer.me

	; <<> DiG 9.8.3-P1 <<>> zonetransfer.me
	;; global options: +cmd
	;; Got answer:
	;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 6133
	;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 2, ADDITIONAL: 1

	;; QUESTION SECTION:
	;zonetransfer.me.		IN	A

	;; ANSWER SECTION:
	zonetransfer.me.	7142	IN	A	217.147.180.162

	;; AUTHORITY SECTION:
	zonetransfer.me.	7142	IN	NS	ns12.zoneedit.com.
	zonetransfer.me.	7142	IN	NS	ns16.zoneedit.com.

	;; ADDITIONAL SECTION:
	ns12.zoneedit.com.	7116	IN	A	209.62.64.46

	;; Query time: 1 msec
	;; SERVER: 213.154.248.156#53(213.154.248.156)
	;; WHEN: Thu Jul  4 10:59:55 2013
	;; MSG SIZE  rcvd: 115

`dig` es la navaja suiza de resolución de nombres y tiene muchas opciones. Proporciona una salida elaborada. El comando `host` ofrece una forma rápida y conveniente de buscar una dirección IP para un host conocido por su nombre.

El nombre de host de una máquina se almacena en un fichero llamado `/etc/hostname` o `/etc/HOSTNAME` para sistemas basados en Debian. En los sistemas Fedora el nombre se almacena en el fichero `/etc/sysconfig/network`. Para todos los sistemas el *hostname* se puede encontrar con el comando `/bin/hostname`. Cuando no se da ningún argumento, este comando da respuestas con el hostname de la máquina. En caso de que un argumento sea dado junto con el comando, el *hostname* de la máquina será cambiado.

### Inicialización incorrecta del sistema

Otra posible causa de problemas en la red puede ser la inicialización incorrecta del sistema. Para encontrar cualquier error de inicialización compruebe el fichero `/var/log/messages` o lea el *kernel ring buffer* usando el comando `/bin/dmesg`.

### Configuración de seguridad

La configuración de seguridad también puede ser una fuente de problemas de conexión. El servidor puede haber bloqueado, o permitido, el acceso desde ciertos clientes usando los ficheros `/etc/host.deny` y `/etc/host.allow`.

### Configuración de la red

Por ejemplo, si los ajustes de la red cableada se copiaron desde otro sitio y no se adaptaron a la situación local. Puede comprobar estos ajustes en los ficheros en el directorio `/etc/sysconfig/network-scripts` para sistemas basados en Fedora, o en el fichero `/etc/network` para sistemas basados en Debian.

### NetworkManager (Gestor de redes)

NetworkManager es una herramienta basada en GUI para administrar sus conexiones de red. NetworkManager es también un servicio que es capaz de reportar cambios en la red. El propósito de NetworkManager es simplificar el uso de la configuración de su red dentro de Linux.

Normalmente, los ajustes de usuario se almacenan en: `/home/$user/.gconf/system/networking/connections`. Los ajustes del sistema se almacenan en: `/etc/Networkmanager/` `/etc/NetworkManager/system-connections`. Tenga en cuenta que NetworkManager sobrescribirá cualquier cambio de configuración realizado en los ajustes de red.

También hay una opción para configurar su NetworManager en la línea de comandos. Se llama `nmcli` y se puede encontrar en `/usr/bin/nmcli`.
