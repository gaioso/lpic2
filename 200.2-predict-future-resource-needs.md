# 200.2 Predecir las necesidades futuras de recursos

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Supervisar la infraestructura de TI](#supervisar-la-infraestructura-de-ti)
    - [collectd](#collectd)
    - [Cacti](#cacti)
    - [MRTG](#mrtg)
        - [Funcionamiento](#funcionamiento)
    - [Nagios](#nagios)
    - [Icinga2](#icinga2)
- [Prededir el conocimiento futuro](#prededir-el-conocimiento-futuro)
- [Saturación de recursos](#saturación-de-recursos)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

Utilizando las herramientas y conocimientos presentados en los capítulos anteriores, debería ser posible diagnosticar el uso de recursos para componentes o procesos específicos. Una de las herramientas mencionadas fue `sar`, que es capaz de registrar las mediciones durante un período de tiempo más largo. La posibilidad de utilizar los datos registrados para el análisis de tendencias es una de las ventajas de utilizar una herramienta que es capaz de registrar las mediciones.

## Supervisar la infraestructura de TI

### collectd

Una de las herramientas que se pueden utilizar para monitorizar una infraestructura de TI es `collectd`. `collectd` es un *daemon/demonio* que recopila estadísticas de rendimiento del sistema periódicamente y proporciona mecanismos para almacenar los valores de diversas maneras. Estas estadísticas se pueden utilizar para encontrar los cuellos de botella de rendimiento actuales (análisis de rendimiento) y predecir la carga futura del sistema (la planificación de la capacidad). `collectd` está escrito en C para el rendimiento y la portabilidad, lo que le permite ejecutarse en sistemas sin lenguaje de *scripting* o *cron*, como los sistemas embebidos. Tenga en cuenta que `collectd` sólo recopila datos, para mostrar los datos recopilados se requieren herramientas adicionales. Durante la instalación se instalará automáticamente también el paquete `rrdtool` para poder leer los informes que estarán en formato `rrd`. Tendrá que editar el archivo `/etc/collectd/collectd.conf` y podra activar *plug-ins*. Los informes se encontrarán en el directorio `/var/lib/collectd`.

### Cacti

Cacti es una completa solución para la generación de gráficos en red, diseñada para aprovechar el poder de almacenamiento y la funcionalidad para gráficas que poseen las aplicaciones *RRDtool*. Esta herramienta, desarrollada en PHP, provee un *pooler* ágil, plantillas de gráficos avanzadas, múltiples métodos para la recopilación de datos, y gestión de usuarios. Tiene una interfaz de usuario fácil de usar, que resulta conveniente para instalaciones del tamaño de una LAN, así como también para redes complejas con cientos de dispositivos.

### MRTG

**MRTG** (Multi Router Traffic Grapher) es una herramienta, escrita en C y Perl por Tobias Oetiker y Dave Rand, que se utiliza para supervisar la carga de tráfico de interfaces de red. MRTG genera un informe en formato HTML con gráficas que proveen una representación visual de la evolución del tráfico a lo largo del tiempo. Para recolectar la información del tráfico del dispositivo (habitualmente routers) la herramienta utiliza el protocolo *SNMP* (Simple Network Management Protocol). Este protocolo proporciona la información en crudo de la cantidad de bytes que han pasado por ellos distinguiendo entre entrada y salida. Esta cantidad bruta deberá ser tratada adecuadamente para la generación de informes. También permite ejecutar cualquier tipo de aplicación en lugar de consultar un dispositivo SNMP. Esta aplicación proporciona como salida dos valores numéricos que se corresponden a la entrada y salida. Habitualmente suelen utilizarse scripts que monitorizan la máquina local. Asimismo, proporciona una aplicación `cfgmaker` que genera la configuración para un router de forma automática utilizando la metainformación que proporciona SNMP.

#### Funcionamiento

MRTG se ejecuta como un demonio o invocado desde las tareas programadas del *cron*. Por defecto, cada cinco minutos recolecta la información de los dispositivos y ejecuta los scripts que se le indican en la configuración. En un primer momento, MRTG consultaba la información, la procesaba y generaba el informe y las gráficas. En las últimas versiones, esta información es almacenada en una base de datos gestionada por RRDtool a partir de la cual, y de forma separada, se generan los informes y las gráficas. Al igual que Cacti, MRTG necesita un servidor web con PHP y MySQL como minimo. Después de la instalación necesitará añadir archivos de configuración:

	# cfgmaker public@192.168.1.1 > /etc/mrtg.cfg

### Nagios

**Nagios** es un sistema de monitorización de redes ampliamente utilizado, de código abierto, que vigila los equipos (hardware) y servicios (software) que se especifiquen, alertando cuando el comportamiento de los mismos no sea el deseado. Entre sus características principales figuran la monitorización de servicios de red (SMTP, POP3, HTTP, SNMP...), la monitorización de los recursos de sistemas hardware (carga del procesador, uso de los discos, memoria, estado de los puertos...), independencia de sistemas operativos, posibilidad de monitorización remota mediante túneles SSL cifrados o SSH, y la posibilidad de programar plugins específicos para nuevos sistemas.

Se trata de un software que proporciona una gran versatilidad para consultar prácticamente cualquier parámetro de interés de un sistema, y genera alertas, que pueden ser recibidas por los responsables correspondientes mediante (entre otros medios) correo electrónico y mensajes SMS, cuando estos parámetros exceden de los márgenes definidos por el administrador de red.

Llamado originalmente *Netsaint*, nombre que se debió cambiar por coincidencia con otra marca comercial, fue creado y es actualmente mantenido por Ethan Galstad, junto con un grupo de desarrolladores de software que mantienen también varios complementos. Nagios fue originalmente diseñado para ser ejecutado en GNU/Linux, pero también se ejecuta bien en variantes de Unix.

### Icinga2

Se trata de un *fork* de Nagios creado en 2009 por un grupo de desarrolladores de la comunidad, que no estaban satisfechos con el desarrollo de dicho proyecto. Es un sistema de monitorización que añade más funcionalidades al *viejo* Nagios, entre las que se encuentra una moderna interfaz web, más adaptada la Web 2.0 También incorpora conectores adicionales para bases de datos (MySQL/MariaDB, Oracle y PostgreSQL) Otra importante mejora es la REST API para que los desarrolladores puedan crear nuevas extensiones.

> **Consejo de examen:** No se esfuerce en la comprensión de los detalles de estas herramientas. Sólo debe conocer la existencia de las mismas sin conocer el proceso de instalación y/o configuración.

## Prededir el conocimiento futuro

Analizando y observando los datos de las mediciones, con el tiempo debería ser posible predecir el crecimiento estadístico de las necesidades de recursos. Decimos deliberadamente *crecimiento estadístico* aquí, porque hay muchas circunstancias que pueden influir en las necesidades de recursos. La demanda de máquinas de fax y líneas telefónicas ha disminuido por la introducción del correo electrónico, por ejemplo. Pero las estimaciones de crecimiento numérico o basado en estadísticas también carecen de linealidad: Al expandirse debido al aumento de la demanda, la expansión suele incorporar una variedad de servicios. La demanda de estos servicios no suele crecer a la misma velocidad para todos los servicios prestados. Esto significa que los datos de las mediciones no sólo deben ser analizados, sino también evaluados en cuanto a su relevancia.

Los pasos para predecir las necesidades futuras se pueden llevar a cabo de la siguiente manera:

* Decidir qué se va a medir.
* Utilice las herramientas apropiadas para medir y registrar los datos relevantes para medir sus objetivos.
* Analizar los resultados de las mediciones, empezando por las mayores fluctuaciones.
* Predecir las necesidades futuras basándose en el análisis.

## Saturación de recursos

Cuando un recurso ya no puede atender la solicitud de forma adecuada, se satura. La demanda y la entrega ya no están alineadas, y la disponibilidad de recursos se convertirá en un problema. La saturación de los recursos puede conducir a una denegación de servicio. Además de interrumpir la disponibilidad de un recurso, los dispositivos que están configurados para «fail open» pueden ser engañados agotando sus recursos. Algunos *switches* vuelven a reenviar todo el tráfico a todos los puertos cuando la tabla ARP se inunda, por ejemplo.

La mayoría de las veces, un solo recurso que se satura será extraíble a partir de los datos de medición recogidos. Esto es lo que llamamos un cuello de botella: un solo punto dentro del sistema reduce el rendimiento y ralentiza todo lo que hay debajo. Es importante tener una clara comprensión de la situación en su conjunto. Simplemente resolver un cuello de botella específico sólo cambiará el problema, si usted aumenta la capacidad de un componente, otro componente se convertirá en el factor limitante tan pronto como alcance su límite de capacidad. Por lo tanto, es importante identificar tantos cuellos de botella como sea posible inmediatamente durante el análisis.

