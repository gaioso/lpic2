# 201.2 - Compilar un kernel

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Obtención del código fuente del kernel](#obtención-del-código-fuente-del-kernel)
    - [Descarga del código fuente](#descarga-del-código-fuente)
- [Limpieza del kernel](#limpieza-del-kernel)
- [Creación de un fichero .config](#creación-de-un-fichero-config)
    - [make config](#make-config)
    - [make menuconfig](#make-menuconfig)
    - [make xconfig y gconfig](#make-xconfig-y-gconfig)
    - [make oldconfig](#make-oldconfig)
- [Compilación del kernel](#compilación-del-kernel)
    - [make dep](#make-dep)
    - [make clean](#make-clean)
    - [make zImage/bzImage](#make-zimagebzimage)
    - [make modules](#make-modules)
    - [make modules_install](#make-modules_install)
- [Instalando el nuevo kernel](#instalando-el-nuevo-kernel)
- [El disco RAM inicial (initrd)](#el-disco-ram-inicial-initrd)
- [Creación manual de initrd](#creación-manual-de-initrd)
- [Crear initrd utilizando mkinitrd (RedHat y derivados)](#crear-initrd-utilizando-mkinitrd-redhat-y-derivados)
- [Crear initrd usando mkinitramfs (Debian y derivados)](#crear-initrd-usando-mkinitramfs-debian-y-derivados)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Obtención del código fuente del kernel

El código fuente del kernel, para casi todas las versiones disponibles, se pueden encontrar en [kernel.org](https://www.kernel.org). Si estamos usando un kernel precompilado para nuestra distribución, existe la posibilidad de que se haya obtenido con la instalación de la distribución, o descargándolo de los repositorios de la misma usando APT, YUM u otra herramienta de mantenimiento.

Los nombres de los ficheros imitan las convenciones de numeración de versiones del kernel. Por ejemplo, el formato del nombre de fichero para las versiones 3.0 y 4.0 es *linux-version.tar.xz*. De este modo, el fichero `linux-3.18.43.tar.xz` es el fichero del kernel para la versión *3.18.43*.

La convención de numeración de versiones utilizada para el kernel 3.0 y 4.0 es **linux-A.B.C.tar.xz**, donde:

* **A** indica la versión del kernel. Solo se incrementa cuando se producen cambios importantes en el código y el concepto.
* **B** indica la revisión.
* **C** es el número de parche.

Los kernel proporcionados por las distribuciones suelen añadir otro número de revisión adicional al número del kernel para llevar la cuenta de los parches aplicados por los gestores de la distribución.

### Descarga del código fuente

Si deseamos obtener la fuente del kernel que está activo en nuestro sistema, podemos usar el comando `uname -r` para conocer su versión y arquitectura.

Incluso si nuestro objetivo no es compilar el kernel, puede resultar de utilidad para consultar la documentación incluída, o porque algunos programas dependen de ficheros de cabecera contenidos en el árbol fuente del kernel para compilarse (como suele ocurrir con *Virtualbox*, por ejemplo).

Una vez obtenida la URL de la versión que deseamos descargar, ejecutamos el siguiente comando para iniciar su descarga:

	# wget https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.19.122.tar.xz

Una ruta común para almacenar y descomprimir las fuentes del kernel es `/usr/src`. Se puede utilizar otra ubicación, siempre que se cree un enlace simbólico desde su nuevo directorio a `/usr/src/linux`.

	# ln -s /usr/src/linux-4.19.122 /usr/src/linux

El código fuente está disponible como un archivo comprimido en formato *xz*. Se puede descomprimir con el comando `unxz`, y luego desempaquetar con el comando `tar`.

	# unxz linux-4.19.122.tar.xz
	# tar xvf linux-4.19.122.tar

También se puede ejecutar el proceso anterior con un solo comando `tar`:

	# tar Jxvf linux-4.19.122.tar.xz

## Limpieza del kernel

Para asegurarse de que comienza con un estado limpio, primero debe *limpiar* el kernel. Cuando compila un kernel en objetos, la utilidad `make` realiza un seguimiento de las cosas y no recompila ningún código que crea que haya sido compilado correctamente antes. En algunos casos, sin embargo, esto puede causar problemas, especialmente cuando se cambia la configuración del kernel. Por lo tanto, es una buena costumbre ejecutar una limipieza (`clean`) del directorio fuente si se reconfigura el kernel.

La limpieza se puede hacer en tres niveles:

* `make clean` (limpiar) 
  * Elimina la mayoría de los fichero generados, pero deja suficiente para construir módulos externos.
* `make mrproper` 
  * Elimina la configuración actual y todos los archivos generados.
* `make distclean` 
  * Elimina los fichero de copia de seguridad del editor, parchea los fichero sobrantes y similares.

> Ejecutar `make mrproper` antes de configurar y construir un kernel es generalmente una buena idea.

> Tenga en cuenta que `make mrproper` también elimina el fichero de configuración principal. Es posible que desee hacer una copia de seguridad de la misma para futuras referencias.

## Creación de un fichero .config

<<<<<<< HEAD
En primer lugar, debemos crear un fichero de configuración. La información de configuración se guarda en el fichero `.config`, donde se establecen los valores para más de 500 opciones, por ejemplo, para diferentes sistemas de ficheros, dispositivos SCSI o soporte de redes. La mayoría de las opciones nos permiten elegir si se han de compilar directamente en el kernel, o bien que se compilen como un módulo. Algunas selecciones implican un grupo de otras selecciones. Por ejemplo, cuando deseamos incluir soporte SCSI, hay opciones adicionales disponibles para controladores y funciones específicas de SCSI.

Algunas de las opciones de soporte del kernel deben ser compiladas como un módulo, otras sólo pueden ser compiladas como parte permanente del kernel y para algunas opciones podrá seleccionar cualquier posibilidad.

Existen varios métodos para configurar el kernel, pero independientemente del método que utilice, los resultados de sus elecciones siempre se almacenan en el fichero de configuración del kernel `/usr/src/linux/.config`. Es un fichero de texto plano que almacena todas las opciones como variables de shell.

Ejemplo de contenido de fichero `.config` para un kernel precompilado en CentOS 8

```
#
# Automatically generated file; DO NOT EDIT.
# Linux/x86_64 4.18.0-147.8.1.el8_1.x86_64 Kernel Configuration
#

#
# Compiler: gcc (GCC) 8.3.1 20190507 (Red Hat 8.3.1-4)
#
CONFIG_64BIT=y
CONFIG_X86_64=y
CONFIG_X86=y
CONFIG_INSTRUCTION_DECODER=y
CONFIG_OUTPUT_FORMAT="elf64-x86-64"
CONFIG_ARCH_DEFCONFIG="arch/x86/configs/x86_64_defconfig"
CONFIG_LOCKDEP_SUPPORT=y
CONFIG_STACKTRACE_SUPPORT=y
CONFIG_MMU=y
CONFIG_ARCH_MMAP_RND_BITS_MIN=28
CONFIG_ARCH_MMAP_RND_BITS_MAX=32
CONFIG_ARCH_MMAP_RND_COMPAT_BITS_MIN=8
CONFIG_ARCH_MMAP_RND_COMPAT_BITS_MAX=16
[...]
```

Se recomienda que no se edite este fichero de forma manual. En su lugar, debemos hacer uso del comando `make` con uno de los cuatro objetivos apropiados para configurar su kernel, y también un compilador C (el más usado es el *GNU-C-Compiler* - `gcc`).

Los cuatro objetivos disponibles son los siguientes:

### make config

Esta ejecución corresponde al enfoque más rudimentario. Tiene claras ventajas y desventajas:
  * No depende de las capacidades de visualización a pantalla completa. Puede utilizarlo en enlaces extremadamente lentos o en sistemas con capacidades de visualización muy limitadas.
  * Tendrá que abrirse camino a través de todas las posibles preguntas sobre las opciones del kernel. El sistema las presentará secuencialmente y sin excepción. Sólo cuando haya respondido a todas las preguntas podrá guardar el archivo de configuración. Dado que, hay muchos cientos de opciones para pasar, se convierte en un método tedioso. Debido a que no es posible desplazarse a través de las diferentes secciones, tendríamos que rehacer los pasos si se comete un error.

Una sesión de ejemplo tendrá el aspecto siguiente:

```
[root@localhost 4.18.0-147.8.1.el8_1.x86_64]# make config
  LEX     scripts/kconfig/zconf.lex.c
  HOSTCC  scripts/kconfig/zconf.tab.o
  HOSTLD  scripts/kconfig/conf
scripts/kconfig/conf  --oldaskconfig Kconfig
*
* Linux/x86 4.18.0-147.8.1.el8_1.x86_64 Kernel Configuration
*
*
* Compiler: gcc (GCC) 8.3.1 20190507 (Red Hat 8.3.1-4)
*
64-bit kernel (64BIT) [Y/n/?] 
*
* General setup
*
Compile also drivers which will not load (COMPILE_TEST) [N/y/?] 
Local version - append to kernel release (LOCALVERSION) [] 
Automatically append version information to the version string (LOCALVERSION_AUTO) [N/y/?] 
Kernel compression mode
> 1. Gzip (KERNEL_GZIP)
  2. Bzip2 (KERNEL_BZIP2)
  3. LZMA (KERNEL_LZMA)
  4. XZ (KERNEL_XZ)
  5. LZO (KERNEL_LZO)
  6. LZ4 (KERNEL_LZ4)
choice[1-6?]: 
Default hostname (DEFAULT_HOSTNAME) [(none)] 
Support for paging of anonymous memory (swap) (SWAP) [Y/n/?] 
System V IPC (SYSVIPC) [Y/n/?] 
POSIX Message Queues (POSIX_MQUEUE) [Y/n/?] 
Enable process_vm_readv/writev syscalls (CROSS_MEMORY_ATTACH) [Y/n/?] 
uselib syscall (USELIB) [N/y/?] 
[...]
```

La mayoría de las consultas ofrecen las mismas posibilidades de respuesta. Estas son:

* **n** 
  * no: el componente correspondiente no se incluirá en el núcleo.
* **y** 
  * si *yes*: el componente correspondiente se compilara en el núcleo.
* **m** 
  * modular: El componente se incluye como un módulo del núcleo y por lo tanto puede ser cargado o descargado más tarde durante la operación.

### make menuconfig

El método `make menuconfig` es más intuitivo y puede ser usado como una alternativa para crear el fichero de configuración. Crea un entorno con ventanas en modo texto basado en las librerías *ncurses*. Puede cambiar de una opción a otra. Las secciones están dispuestas en una estructura de menú que es fácil de navegar y puede guardar y salir cuando lo desee. Si prefiere una combinación de colores más oscuros, utilice `make nconfig`.

![image](images/make-menuconfig.png)

Cuando haya terminado, utilice las teclas de flecha para seleccionar la opción *Exit* en la parte inferior de la pantalla. Si se ha realizado algún cambio, se le preguntará si desea guardar la nueva configuración. También puede elegir guardar la configuración usando otro nombre y/o ubicación en el sistema de ficheros.

> Si elige otro nombre o ubicación, deberá mover el fichero `.config` al directorio `/usr/src/linux` para poder compilar el kernel.

### make xconfig y gconfig

El comando `make xconfig` abre un menú gráfico desde el que configurar el kernel. Es necesario disponer de un sistema *X Window* operativo, y las librerías de desarrollo Qt. Nos proporcionará un menú a través del que podremos navegar haciendo uso del ratón. Si deseamos usar *gnome* en lugar de Qt, debemos ejecutar `make gconfig`. Por supuesto, para usar esta opción debemos disponer de las librerías de desarrollo *GTK+2.x*.

En la siguiente captura se puede visualizar el menú raíz de la ventana del comando `make xconfig`:

![image](/images/xconfig.jpg)

Y, el comando `make gconfig` hace exactamente lo mismo, pero usa GTK en lugar de QT:

![image](/images/gconfig.jpg)

### make oldconfig

En realidad, esto no es un método de configuración. Se usa para copiar la configuración de un kernel ya existente en un fichero `.config` y así mantener las opciones que elija durante la compilación. De esta forma evitamos el tener que comenzar desde cero la configuración.

Debemos asegurarnos que el fichero `.config`, que fue el resultado de la compilación anterior, se copia en el directorio `/usr/src/linux`. Cuando se ejecute `make oldconfig`, el fichero `.config` se moverá a `.config.old` y se creará un nuevo fichero `.config`. Se nos pedirán los valores a las variables que no se encuentran en el fichero de configuración anterior.

> Debemos asegurarnos de hacer una copia de seguridad de `.config` antes de actualizar el código fuente del kernel, ya que la distribución puede contener un fichero `.config` predeterminado, sobreescribiendo su fichero anterior.
> Los comandos `make xconfig`, `make gconfig` y `make menuconfig` utilizarán de forma automática el fichero antiguo `.config` (si está disponible) para poder crear un nuevo, conservando tantas opciones como sea posible y añadiendo nuevas opciones utilizando sus valores por defecto.

## Compilación del kernel

Una vez finalizada la configuración, se puede iniciar el proceso de compilación del kernel y preparar el sistema para su utilización. El proceso de montaje es bastante sencillo, si bien después de copiar el fichero del kernel en `/boot`, instalar los módulos del kernel, preparar un disco RAM inicial y modificar su configuración GRUB para que apunte al nuevo kernel. Puede que también se quiera preparar un archivo de paquetes.

Usaremos la siguiente secuencia de comandos `make` para construir e instalar el kernel y los módulos:

1. make dep
2. make clean
3. make zImage/bzImage
4. make modules
5. make modules_install

### make dep

Este comando sirve para la creación adecuada de las dependencias necesarias para la compilación del núcleo.

### make clean

El argumento *clean* elimina los viejos ficheros de salida que pueden existir de las versiones anteriores del kernel. Estos incluyen ficheros de kernel, archivos de mapa de sistema y otros.

### make zImage/bzImage

Los argumentos *zImage* y *bzImage* construyen el kernel de forma efectiva. La diferencia entre estos dos se explica en el tema 201.1 Después del proceso de compilación, la imagen del kernel se puede encontrar en el directorio `/usr/src/linux/arch/i386/boot` (en sistemas i386).

### make modules

El argumento *modules* construye los módulos; los controladores de dispositivo y otros elementos que fueron configurados como módulos.

### make modules_install

El argumento *modules_install* instala los módulos que acaba de compilar en `/lib/modules/kernel-version`. El directorio de la versión del kernel se creará si no existe.

## Instalando el nuevo kernel

Una vez compilado el nuevo kernel, el sistema puede configurarse para poder arrancarlo. Primero necesita poner una copia de la nueva *bzImage* en el directorio de arranque (que debería residir en su propia partición de arranque). Para mayor claridad, el nombre del fichero del kernel debe contener el número de versión del kernel, por ejemplo: `vmlinuz-2.6.31`:

	# cp /usr/src/linux/arch/x86_64/boot/bzImage /boot/vmlinuz-2.6.31

Esto también asegura que puede tener más de una versión del kernel en el directorio `/boot`, por ejemplo si necesita arrancar un kernel antiguo debido a problemas de compatibilidad con el nuevo. Después de mover el fichero del kernel a la ubicación correcta, necesitará configurar el gestor de arranque (GRUB) para que pueda arrancar con el nuevo kernel.

Para obtener información más específica sobre GRUB, consulte la sección GRUB en el tema 202.2.

## El disco RAM inicial (initrd)

Digamos que su disco de arranque tiene el *bootloader* (cargador de arranque), el kernel y los módulos adecuados. Dadas las ventajas de los módulos del kernel, hemos decidido usarlos. Pero si también los quiere usar para los controladores del dispositivo de *boot* (arranque), se enfrenta a un problema. GRUB cargará el kernel, y luego lo ejecutará. El kernel intentará acceder al disco para obtener los módulos. Sin embargo, como aún no ha cargado el módulo adecuado, no puede acceder a ese disco y se bloquea.

Una solución perfectamente válida sería construir un kernel con el disco duro necesario. Pero si tiene que mantener un mayor número de sistemas diferentes, necesita una configuración y un kernel personalizados para cada tipo de sistema, o tiene que vivir con un kernel de gran tamaño. Para evitar todos estos problemas, los desarrolladores del kernel encontraron una solución: **el disco RAM initrd**.

Un disco RAM es un espacio de memoria que el kernel ve como si fuera un disco. Se puede montar como cualquier otro disco. El kernel soporta discos RAM por defecto. GRUB también puede manejar discos RAM. Puede ordenarle que carguen el disco RAM desde un fichero y cuando el kernel arranca tendrá el disco RAM disponible. Estos discos RAM se utilizan a menudo para alojar scripts y módulos que facilitan el proceso de arranque.

Por convención, el nombre de la imagen que contiene el disco RAM inicial es `initrd`. El nombre es la abreviatura de «disco RAM inicial».

El cargador de arranque carga el `initrd`, es montado por el kernel como su sistema de ficheros raíz. Una vez montado como el sistema de ficheros raíz, los programas se pueden ejecutar desde él, y los módulos del kernel se pueden cargar desde él. Después de este paso se puede montar un nuevo sistema de ficheros raíz desde un dispositivo diferente. La raíz anterior (de initrd) se mueve al directorio `/initrd`, o se desmonta.

Hay varias maneras de crear su propio fichero `initrd`. Un método muy conveniente, utilizado principalmente por las distribuciones de Red Hat (y derivadas) es el uso del script `mkinitrd`. Es un script de shell que se puede inspeccionar para ver cómo funciona. En distribuciones basadas en Debian se puede usar una utilidad llamada `mkinitramfs` para el mismo propósito. También se puede optar por construir el fichero a mano, tal y como se muestra en el capítulo siguiente.

## Creación manual de initrd

Los ficheros `initrd` que son ficheros comprimidos contienen los ficheros de un sistema de ficheros raíz mínimo. Este sistema de ficheros raíz normalmente contiene módulos, scripts y algunos binarios adicionales necesarios para permitir que el kernel continúe correctamente su arranque.

Como ya se ha dicho, el script `mkinitrd` ofrece una forma conveniente de construir el archivo `initrd`, aunque no todas las distribuciones lo proporcionan. Si quieres (o debes) construir uno a mano, los pasos son: crear un sistema de ficheros raíz, rellenarlo con módulos y ficheros, crear un archivo *tar* o *cpio* a partir de él y, por último, comprimirlo en *gzip*.

El tipo de archivo a utilizar depende de la distribución y de la versión del kernel. Los kernels más antiguos emplean *tar*, mientras que los más recientes utilizan *cpio*. Si no está seguro y tiene un `initrd` a mano que vino con su distribución, puede usar una secuencia de comandos como la que se muestra a continuación para comprobarlo:

	$ sudo zcat initramfs-4.18.0-147.8.1.el8_1.x86_64.img | file -
	/dev/stdin: ASCII cpio archive (SVR4 with no CRC)

	# lsinitrd initramfs-4.18.0-147.8.1.el8_1_x86_64.img

El ejemplo anterior muestra la salida de una distribución CentOS 8 que utiliza `cpio` como su herramienta de archivo.

Para poder trabajar con las imágenes `initrd`, el kernel debe estar compilado con soporte para el disco RAM y configurado de tal forma que lo utilice. Independientemente de lo que ponga en el disco RAM inicial, debería ser compatible con el kernel y la arquitectura que vaya a utilizar. Por ejemplo, su kernel de arranque debería ser capaz de reconocer el tipo de sistema de ficheros utilizado en la imagen, y los módulos que incluya deberían coincidir con la versión del kernel de arranque.

El siguiente paso es crear la imagen del disco RAM. Primero cree un sistema de ficheros en un dispositivo de bloque, y luego copie los archivos a ese sistema de ficheros según sea necesario. Los dispositivos de bloque adecuados para este fin son:

* Un disco RAM (rápido, asigna memoria física)
* Un dispositivo de loopback (ligeramente más lento, asigna espacio en disco)

En el resto de este ejemplo usaremos el método del disco RAM, por lo que tendremos que asegurarnos de que haya un nodo de dispositivo de disco RAM presente (puede haber más de uno):

	# ls -la /dev/ram0
	brw-rw---- 1 root disk 1,  0 Feb 13 00:18 /dev/ram0 

> Nota: El número de discos RAM que están disponibles por defecto en un sistema es una opción en la configuración del kernel: **CONFIG_BLK_DEV_RAM_COUNT**.

A continuación, se debe crear un sistema de ficheros vacío del tamaño apropiado:

	# mke2fs -m0 /dev/ram0 300

> Nota: Si el espacio es crítico, puede usar un sistema de ficheros que sea más eficiente con el espacio, como el *Minix FS*. Recuerde que el boot-kernel (kernel de arranque) necesitará soporte integrado para cualquier sistema de ficheros que elija.

Después de haber creado el sistema de ficheros, necesita montarlo en el directorio apropiado:

	# mount -t ext2 /dev/ram0 /mnt

Ahora es necesario crear el talón del dispositivo de consola. Este será el nodo del dispositivo que se utilizará cuando el `initrd` esté activo.

	# mkdir /mnt/dev
	# mknod /mnt/dev/tty1 c 4 1
			
A continuación, copie todos los ficheros que considere necesarios para la imagen; módulos, scripts, binarios, no importa. Remítase a Contents (Contenidos) de /, /boot y /lib/modules para ver un ejemplo de los directorios y ficheros necesarios como mínimo. Uno de los ficheros más importantes para copiar es `/linuxrc`. Cada vez que el kernel está configurado para usar una imagen `initrd` buscará un fichero `/linuxrc` y lo ejecutará. Puede ser un script o un binario compilado. Por lo tanto, lo que sucederá después de montar su archivo de imagen está totalmente bajo su control. En este ejemplo haremos de `/linuxrc` un enlace a `/bin/sh`. Asegúrese de que `/linuxrc` tenga permisos de ejecución.

	# ln -s /bin/sh /mnt/linuxrc
			
Una vez que haya terminado de copiar los ficheros y se haya asegurado de que `/linuxrc` tiene los atributos correctos, puede desmontar la imagen del disco RAM:

	# umount /dev/ram0

La imagen del disco RAM puede copiarse a un fichero:

	# dd if=/dev/ram0 bs=1k count=300 of=/boot/initrd
			
Por último, si ya no utiliza el disco RAM y desea recuperar la memoria, deslocalice el disco RAM:

	# freeramdisk /dev/ram0
			
Para probar el `initrd` recién creado, agregue una nueva sección a su fichero de configuración de GRUB, que se refiere a la imagen `initrd` que acaba de crear:

	title=initrd test entry
	root (hd0,0)
	kernel /vmlinuz-2.6.28
	initrd /initrd
			
Si ha seguido los pasos anteriores y ha reiniciado usando esta entrada de prueba desde el menú *bootloader*, el sistema continuará arrancando. Después de unos segundos debería encontrarse en una línea de comandos, ya que `/linuxrc` se refiere a `/bin/sh`, una shell.

Por supuesto, los ficheros `initrd` reales contendrán un fichero de arranque `/linuxrc` más complejo, que carga módulos, monta el sistema de ficheros raíz real, etc.

## Crear initrd utilizando mkinitrd (RedHat y derivados)

> Nota: `mkinitrd` fue discutido en una sección anterior, que también incluye cómo crear tal imagen manualmente.

Para limitar el tamaño del kernel, a menudo se utilizan imágenes iniciales de ramdisk (initrd) para precargar los módulos necesarios para acceder al sistema de ficheros raíz.

El `mkinitrd` es una herramienta específica para distribuciones basadas en RPM (como Red Hat, SuSE, etc.). Esta herramienta automatiza el proceso de creación de un archivo `initrd`, asegurando así que el proceso relativamente complejo se siga correctamente.

En la mayoría de las distribuciones más importantes de Linux, la imagen `initrd` contiene casi todos los módulos necesarios del kernel; muy pocos serán compilados directamente en el kernel. Esto permite el despliegue de correcciones y parches fáciles en el kernel y sus módulos a través de paquetes RPM: una actualización de un único módulo no requerirá una recompilación o sustitución de todo el kernel, sino sólo del único módulo, o en el peor de los casos de algunos módulos dependientes. Debido a que estos módulos están contenidos dentro del fichero `initrd`, este fichero necesita ser regenerado cada vez que se recompila (manualmente) el kernel, o se instala un patch del kernel (módulo). 

Generar una nueva imagen `initrd` es muy sencillo si se utiliza la herramienta estándar proporcionada en muchas distribuciones:

	# mkinitrd initrd-image kernel-version

Las opciones útiles para `mkinitrd` incluyen:

* **--version** 
  * Esta opción muestra el número de versión de la utilidad `mkinitrd`.
* **-f** 
  * Al especificar este parámetro, la utilidad sobrescribirá cualquier fichero de imagen existente con el mismo nombre.
* **--builtin=** 
  * Esto hace que `mkinitrd` asuma que el módulo especificado fue compilado en el kernel. No buscará el módulo y no mostrará ningún error si no existe.
* **--omit-lvm-modules, --omit-raid-modules, --omit-scsi-modules** 
  * Usando estas opciones es posible evitar la inclusión de, respectivamente, módulos LVM, RAID o SCSI, incluso si están presentes, o la utilidad los incluiría normalmente basándose en el contenido de `/etc/fstab` y/o `/etc/raidtab`.

## Crear initrd usando mkinitramfs (Debian y derivados)

Insatisfecho con la herramienta que utilizan las distribuciones basadas en RPM (`mkinitrd`), algunos desarrolladores de Debian escribieron otra utilidad para generar un fichero initrd. Esta herramienta se llama `mkinitramfs`. La herramienta `mkinitramfs` es un script de shell que genera una imagen gzip. Fue diseñado para ser mucho más simple (tanto en código como en uso) que `mkinitrd`. El guión consta de unas 380 líneas de código.

La configuración de `mkinitramfs` se realiza a través de un fichero de configuración: `initramfs.conf`. Este fichero se encuentra normalmente en `/etc/initramfs-tools/initramfs.conf`. Este fichero de configuración es originado por el script - contiene declaraciones estándar de bash. Los comentarios van precedidos por un `#`. Las variables se especifican mediante:

	variable=value

Las opciones que se pueden utilizar con `mkinitramfs` incluyen:

* **-d confdir** 
  * Esta opción establece un directorio de configuración alternativo.
* **-k** 
  * «Mantener» el directorio temporal utilizado para crear la imagen.
* **-o outfile** 
  * Escriba la imagen resultante en el archivo.
* **-r root** 
  * Sobrescribe la configuración ROOT en el archivo `initramfs.conf`.

> Nota: En las distribuciones basadas en Debian debería usar siempre `mkinitramfs` ya que `mkinitrd` no es válido para los kernels más recientes.
