# 202.1 - Personalizar el inicio del sistema

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Crear initrd usando mkinitrd](#crear-initrd-usando-mkinitrd)
- [Crear initrd usando mkinitramfs](#crear-initrd-usando-mkinitramfs)
- [Configurar el dispositivo raíz](#configurar-el-dispositivo-raíz)
- [El proceso de arranque de Linux](#el-proceso-de-arranque-de-linux)
- [El proceso init](#el-proceso-init)
    - [Resumen](#resumen)
- [Configurando /etc/inittab](#configurando-etcinittab)
    - [Descripción de una entrada en /etc/inittab](#descripción-de-una-entrada-en-etcinittab)
- [El script /etc/init.d/rc](#el-script-etcinitdrc)
- [update-rc.d](#update-rcd)
- [Uso de los systemd targets](#uso-de-los-systemd-targets)
- [El estándar LSB](#el-estándar-lsb)
    - [El entorno bootscript y comandos](#el-entorno-bootscript-y-comandos)
- [Cambio y configuración de los runlevels](#cambio-y-configuración-de-los-runlevels)
- [El comando chkconfig](#el-comando-chkconfig)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Crear initrd usando mkinitrd

> Nota: `mkinitrd` se discutió en un [tema anterior](/201.2-compiling-a-linux-kernel.html#el-disco-ram-inicial-initrd), que también abarca la creación de la imagen de forma manual.
 
Para limitar el tamaño del núcleo, a menudo se utilizan imágenes de ramdisk (initrd) para precargar los módulos necesarios que permitan el acceso al sistema de ficheros raíz.

El comando `mkinitrd` es una herramienta específica para las distribuciones basadas en RPM (como Red Hat, SuSE, etc.). Esta herramienta automatiza el proceso de creación de un fichero `initrd`, asegurándose así de que el proceso relativamente complejo se completa correctamente.

En la mayoría de las grandes distribuciones de GNU/Linux, la imagen `initrd` contiene casi todos los módulos necesarios del kernel; muy pocos serán compilados directamente en el kernel. Esto permite el despliegue sencillo de parches al kernel y sus módulos a través de paquetes RPM: una actualización de un solo módulo no requerirá una recompilación o reemplazo de todo el kernel, sino sólo del módulo individual, o en el peor de los casos de unos pocos módulos dependientes. Debido a que estos módulos están contenidos dentro del fichero `initrd`, este fichero necesita ser regenerado cada vez que el kernel es recompilado (manualmente), o se instala un parche para el kernel (módulo). La generación de una nueva imagen `initrd` es muy sencilla si se utiliza la herramienta estándar que se proporciona en la mayoría de distribuciones:

	# mkinitrd initrd-image kernel-version
                         
Las opciones útiles para `mkinitrd` incluyen:

* **--versión** - Esta opción muestra el número de versión de `mkinitrd`.
* **-f** - Al especificar este interruptor, la utilidad sobrescribirá cualquier fichero de imagen existente con el mismo nombre.
* **--builtin=** - Esto hace que `mkinitrd` asuma que el módulo especificado fue compilado en el núcleo. No buscará el módulo y no mostrará ningún error si no existe.
* **--omit-lvm-modules, --omit-raid-modules, --omit-scsi-modules** - Utilizando estas opciones es posible evitar la inclusión de, respectivamente, los módulos LVM, RAID o SCSI, aunque estén presentes, o la utilidad normalmente los incluiría basándose en el contenido de `/etc/fstab` y/o `/etc/raidtab`.
 
## Crear initrd usando mkinitramfs

Insatisfechos con la herramienta que usan las distribuciones basadas en RPM (mkinitrd), algunos desarrolladores de Debian desarrollaron una nueva utilidad para generar el fichero `initrd`. Esta herramienta se llama `mkinitramfs`. Esta utilidad es un script de shell que genera una imagen *cpio* comprimida en *gzip*. Fue diseñado para ser mucho más simple (tanto en el código como en el uso) que `mkinitrd`. El script está formado por unas 380 líneas de código.

La configuración de `mkinitramfs` se hace a través de un fichero de configuración: `initramfs.conf`. Este fichero se encuentra normalmente en `/etc/initramfs-tools/initramfs.conf`. Este fichero de configuración es procesado en el script - contiene declaraciones *bash* estándar. Los comentarios llevan el prefijo `#.` Las variables se especifican con:

	variable=valor
                         
Las opciones que se pueden usar con `mkinitramfs` incluyen:

* **-d confdir** - Esta opción establece un directorio de configuración alternativo.
* **-k** - *Mantener* el directorio temporal usado para crear la imagen.
* **-o outfile** - Envía la imagen resultante a un fichero externo.
* **-r root** - Anula la configuración *ROOT* en el fichero `initramfs.conf`. 

> Nota: En las distribuciones derivadas de Debian se debe usar `mkinitramfs`, ya que `mkinitrd` no está disponible para los núcleos más recientes.

## Configurar el dispositivo raíz

La configuración del dispositivo raíz es una de las muchas configuraciones del kernel. La configuración del kernel se origina desde, o puede ser sobrescrita por:

* valores predeterminados tal y como se establece en el código fuente
* por defecto como lo establece el comando `rdev`
* valores pasados al kernel en el momento del arranque, por ejemplo `root=/dev/xyz`
* especificados en el fichero de configuración de GRUB.

Los más obvios son los dispositivos de bloques como discos duros, almacenamiento SAN, CDs o DVDs. Incluso puede tener un disco raíz montado en NFS, lo que requiere el uso de `initrd` y la configuración de las opciones de arranque `nfs_root_name` y `nfs_root_addrs`. Puede configurar o cambiar el dispositivo raíz a casi cualquier cosa desde el entorno `initrd`. Para ello, asegúrese de que el sistema de ficheros `/proc` haya sido montado por los scripts de la imagen `initrd`. Los siguientes ficheros están disponibles en `/proc`:

        /proc/sys/kernel/real-root-dev
        /proc/sys/kernel/nfs-root-name
        /proc/sys/kernel/nfs-root-addrs

El `real-root-dev` se refiere al número de nodo del dispositivo del sistema de ficheros raíz. Se puede cambiar fácilmente escribiendo el nuevo número:

        # echo 0x301>/proc/sys/kernel/real-root-dev

Esto cambiará la raíz real del sistema de ficheros en `/dev/hda1`. Si desea utilizar una raíz montada en NFS, los ficheros `nfs-root-name` y `nfs-root-addrs` deben configurarse utilizando los valores apropiados y el número de dispositivo debe establecerse a `0xff`:

        # echo /var/nfsroot >/proc/sys/kernel/nfs-root-name
        # echo 193.8.232.2:193.8.232.7::255.255.255.0:idefix \
           >/proc/sys/kernel/nfs-root-addrs
        # echo 255>/proc/sys/kernel/real-root-dev

> Nota: Si el dispositivo raíz está configurado en el disco RAM, el sistema de ficheros raíz no se mueve a `/initrd`, sino que el procedimiento de arranque simplemente continúa iniciando `init` en el disco RAM inicial.

## El proceso de arranque de Linux

Hay siete fases distinguibles durante el arranque:

    1. Carga, configuración y ejecución del cargador de kernels
    2. Configuración del registro
    3. Descompresión del kernel
    4. Inicialización del kernel y de la memoria
    5. Configuración del kernel
    6. Habilitación de las CPUs restantes
    7. Iniciar la creación del proceso

El proceso de arranque se describe en detalle en [The Kernel Boot Process](https://manybutfinite.com/post/kernel-boot-process/) de Gustavo Duarte. En el último paso en el proceso de arranque, el kernel intenta ejecutar estos comandos en orden, hasta que uno de ellos tiene éxito:

    1. /sbin/init
    2. /etc/init
    3. /bin/init
    4. /bin/sh

Si ninguno de ellos tiene éxito, el kernel entrará en «modo pánico».

## El proceso init

`init` es el padre de todos los procesos, y lee su configuración del fichero `/etc/inittab` para crear los procesos basándose en su contenido. Una de las tareas que suele realizar es levantar las consolas *gettys* para que los usuarios se puedan conectar. También define los *niveles de ejecución*.

Un nivel de ejecución es una configuración del sistema que permite solo la existencia de un determinado grupo de procesos.

`init` puede encontrarse en uno de los siguientes ocho niveles de ejecución:

* **runlevel 0 (reservado)** - El nivel de ejecución 0 se utiliza para detener el sistema.
* **runlevel 1 (reservado), s y S** - El nivel de ejecución 1 se utiliza para poner el sistema en modo monousuario y sin conectividad a la red. Solo permite la conexión de *root* y tiene los servicios mínimos cargados. Es perfecto para tareas de mantenimiento sin interferir con la producción.
* **runlevel 2** - Este nivel no está definido por igual en cada distribución Linux. Pero se pueden observar puntos en común como que no posee una interfaz gráfica pero si posee la función de multiusuario local y además los servicios de red están activados.
* **runlevel 3** - Este tampoco está definido en todas las distribuciones por igual. Aun asi en la mayoria de casos coincide con el *runlevel 2* añadiendo a la funcionalidad de multiusuario no local, (remotos) y además en muchas distribuciones empiezan a iniciarse las interfaces gráficas.
* **runlevel 4** - Ninguna distribución conocida la utiliza
* **runlevel 5** - Es el nivel con mas funcionalidades y ofrece acceso a los servicios de red, uso de multiusuario y interfaz grafica.
* **runlevel 6** - El nivel de ejecución 6 se utiliza para reiniciar el sistema.

### Resumen

* runlevel 0 - Parar/apagar el sistema
* runlevel 1 - Monousuario (root y sin red)
* runlevel 2 - Multiusuario (local) sin red remota
* runlevel 3 - multiusuaio (local y no local) con red
* runlevel 4 - No se usa
* runlevel 5 - Funcionalidad completa de multiusuario, red y xdm
* runlevel 6 - Reinicio de sistema

## Configurando /etc/inittab

Como se mencionó antes, `init` lee el fichero `/etc/inittab` para determinar qué debe hacer. Una entrada en este fichero tiene el siguiente formato:

	id:runlevels:action:process (id:niveles de ejecución:acción:proceso)

A continuación se incluye un ejemplo de fichero `/etc/inittab`.

```
	# The default runlevel.
	id:2:initdefault:

	# Boot-time system configuration/initialization script.
	# This is run first except when booting in emergency (-b) mode.
	si::sysinit:/etc/init.d/rcS

	# What to do in single-user mode.
	~~:S:wait:/sbin/sulogin

	# /etc/init.d executes the S and K scripts upon change
	# of runlevel.
	#
	# Runlevel 0 is halt.
	# Runlevel 1 is single-user.
	# Runlevels 2-5 are multi-user.
	# Runlevel 6 is reboot.

	l0:0:wait:/etc/init.d/rc 0
	l1:1:wait:/etc/init.d/rc 1
	l2:2:wait:/etc/init.d/rc 2
	l3:3:wait:/etc/init.d/rc 3
	l4:4:wait:/etc/init.d/rc 4
	l5:5:wait:/etc/init.d/rc 5
	l6:6:wait:/etc/init.d/rc 6
	# Normally not reached, but fall through in case of emergency.
	z6:6:respawn:/sbin/sulogin

	# /sbin/getty invocations for the runlevels.
	#
	# The "id" field MUST be the same as the last
	# characters of the device (after "tty").
	#
	# Format:
	#  <id>:<runlevels>:<action>:<process>
	1:2345:respawn:/sbin/getty 38400 tty1
	2:23:respawn:/sbin/getty 38400 tty2
```

### Descripción de una entrada en /etc/inittab

* **id** - Es una secuencia única de 1 a 4 caracteres que identifican una entrada de forma unívoca. Sin embargo, para *gettys* y otros procesos de inicio de sesión, el campo `id` debe contener el sufijo de la *tty* correspondiente, de lo contrario la contabilidad de inicio de sesión podría no funcionar.
* **runlevels** - Es la lista de niveles de ejecución para lo cuales se llevarán a cabo las acciones especificadas.
* **action** - Describe qué acción se debería llevar a cabo. Estas acciones pueden ser:
  * **respawn** - El proceso se reiniciará cuando termine (por ejemplo, getty).
  * **wait** - El proceso se iniciará una vez cuando se introduzca el nivel de ejecución especificado e `init` esperará a su finalización.
  * **once** - El proceso se ejecutará una vez cuando se introduzca el nivel de ejecución especificado.
  * **boot** - El proceso se ejecutará durante el arranque del sistema. Se ignora el campo *runlevelels*.
  * **bootwait** - El proceso se ejecutará durante el arranque del sistema, mientras que `init` espera su terminación (por ejemplo, `/etc/rc`). Se ignora el campo *runlevelels*.
  * **ondemand** - Un proceso marcado con un runlevel ondemand (bajo demanda) se ejecutará siempre que se llame al nivel de ejecución bajo demanda especificado. Sin embargo, no se producirá ningún cambio de nivel de ejecución (los runlevels bajo demanda son «a», «b» y «c»).
  * **initdefault** - Una entrada `initdefault` especifica el nivel de ejecución que se debe iniciar después del arranque del sistema. Si no existe ninguno, `init` pedirá un nivel de ejecución en la consola. El campo de proceso se ignora. En el ejemplo anterior, el sistema pasará al nivel de ejecución 5 después del arranque.
  * **sysinit** - El proceso se ejecutará durante el arranque del sistema. Se ejecutará antes de cualquier entrada de arranque o `bootwait`. Se ignora el campo *runlevelels*.
  * **powerwait** - El proceso se ejecutará cuando se interrumpa el suministro eléctrico. `init` suele ser informado de ello mediante un proceso que habla con un SAI conectado al ordenador. `init` esperará a que el proceso finalice antes de continuar.
  * **powerfail** - Como `powerwait`, excepto que `init` no espera a que el proceso se complete.
  * **powerokwait** - Este proceso se ejecutará tan pronto como `init` sea informado de que la energía ha sido restablecida.
  * **powerfailnow** - Este proceso se ejecutará cuando se indique que la batería del SAI externo está casi vacía y que la alimentación está fallando (siempre que el SAI externo y el proceso de monitorización sean capaces de detectar esta condición).
  * **ctrlaltdel** - El proceso se ejecutará cuando `init` reciba la señal **SIGINT**. Esto significa que alguien en la consola del sistema ha presionado la combinación de teclas **CTRL-ALT-DEL**. Típicamente uno quiere ejecutar algún tipo de apagado ya sea para entrar en el nivel de usuario único o para reiniciar la máquina.
  * **kbdrequest** - El proceso se ejecutará cuando `init` reciba una señal del controlador del teclado de que se ha pulsado una combinación especial de teclas en el teclado de la consola. Básicamente, desea asignar alguna combinación de teclas a la acción «KeyboardSignal». Por ejemplo, para mapear `Alt-Uparrow` para este propósito use lo siguiente en su fichero de mapas de teclas: `alt keycode 103 = KeyboardSignal`.
* **process** - Este campo especifica el proceso que debe ejecutarse. Si el campo de proceso comienza con un «+», `init` no hará la contabilización de `utmp` y `wtmp`. Algunos *gettys* insisten en hacer sus propias *tareas domésticas*.

## El script /etc/init.d/rc

Para cada uno de los niveles de ejecución 0-6 hay una entrada en `/etc/inittab` que ejecuta `/etc/init.d/rc?` donde «?» es 0-6, como se puede comprobar en la línea del siguiente ejemplo:

	l2:2:wait:/etc/init.d/rc 2

Entonces, lo que realmente sucede es que `/etc/init.d/rc` se ejecuta con el *runlevel* como parámetro.

El directorio `/etc` contiene varios directorios, específicos de nivel de ejecución, que a su vez contienen enlaces simbólicos específicos de nivel de ejecución a scripts en `/etc/init.d/`. Esos directorios son:

	$ ls -d /etc/rc*
	/etc/rc.boot  /etc/rc1.d  /etc/rc3.d  /etc/rc5.d  /etc/rcS.d
	/etc/rc0.d    /etc/rc2.d  /etc/rc4.d  /etc/rc6.d

Como se puede ver, también hay un directorio `/etc/rc.boot`. Este directorio está obsoleto y ha sido reemplazado por el directorio `/etc/rcS.d`. En el momento del arranque, el directorio `/etc/rcS.d` es analizado primero y luego, para compatibilidad con versiones anteriores, el directorio `/etc/rc.boot`.

El nombre del enlace simbólico comienza con una «S» o con una «K». Examinemos el directorio `/etc/rc2.d`:

```
        $ ls /etc/rc2.d
        K20gpm          S11pcmcia       S20logoutd      S20ssh          S89cron
        S10ipchains     S12kerneld      S20lpd          S20xfs          S91apache
        S10sysklogd     S14ppp          S20makedev  S22ntpdate          S99gdm
        S11klogd        S20inetd        S20mysql        S89atd          S99rmnologin
```

Si el nombre del enlace simbólico comienza con una «K», el script se llama con *stop* como parámetro para detener el proceso. Este es el caso de `K20gpm`, por lo que el comando se convierte en `K20gpm stop`. Averigüemos cómo se llama el programa o script:

        $ ls -l /etc/rc2.d/K20gpm
        lrwxrwxrwx 1 root root 13 Mar 23 2001 /etc/rc2.d/K20gpm -> ../init.d/gpm

Así, `K20gpm stop` se traduce como `/etc/init.d/gpm stop`. Veamos qué pasa con el parámetro *stop* examinando parte del script:

```
        #!/bin/sh
        #
        # Start Mouse event server
        ...
        case "$1" in
        start)
           gpm_start
           ;;
        stop)
           gpm_stop
           ;;
        force-reload|restart)
           gpm_stop
           sleep 3
           gpm_start
           ;;
        *)
           echo "Usage: /etc/init.d/gpm {start|stop|restart|force-reload}"
           exit 1
        esac
```

En este caso, `esac` examina el primer parámetro, `$1`, y en el caso de que su valor sea *stop*, se ejecuta `gpm_stop`.

Por otro lado, si el nombre del enlace simbólico comienza con una «S», el script se llama con *start* como parámetro para iniciar el proceso.

Los scripts se ejecutan en un orden léxico según los nombres de fichero.

Digamos que tenemos un daemon `SomeDaemon`, un script relacionado `/etc/init.d/SDscript` y queremos que `SomeDaemon` se ejecute cuando el sistema está en el nivel de ejecución 2, pero no cuando el sistema está en el nivel de ejecución 3.

Como ya sabemos, esto significa que necesitamos un enlace simbólico, empezando con una «S», para el nivel de ejecución 2, y un enlace simbólico, empezando con una «K», para el nivel de ejecución 3. También hemos determinado que el daemon `SomeDaemon` debe iniciarse después de `S19someotherdaemon`, lo que implica que debemos usar `S20` y `K80`, ya que el *start/stop* es simétrico, es decir, que lo que se inicia primero se detiene al final. Esto se logra con el siguiente conjunto de comandos:

	# cd /etc/rc2.d
        # ln -s ../init.d/SDscript S20SomeDaemon
        # cd /etc/rc3.d
        # ln -s ../init.d/SDscript K80SomeDaemon

Si desea iniciar, reiniciar o detener manualmente un proceso, es una buena práctica utilizar el script apropiado en `/etc/init.d/`, por ejemplo `/etc/init.d/gpm restart` para ejecutar el reinicio del proceso.

## update-rc.d

> Nota: Esta sección sólo se aplica a las distribuciones Debian y derivadas

Las distribuciones derivadas de Debian usan el comando `update-rc.d` para instalar y eliminar los enlaces de script mencionados en la sección anterior.

Si tiene un script de inicio llamado «foobar» en `/etc/init.d/` y quiere añadirlo a los *runlevels* por defecto, puede utilizar:

        # update-rc.d foobar defaults
        Adding system startup for /etc/init.d/foobar ...
         /etc/rc0.d/K20foobar -> ../init.d/foobar
         /etc/rc1.d/K20foobar -> ../init.d/foobar
         /etc/rc6.d/K20foobar -> ../init.d/foobar
         /etc/rc2.d/S20foobar -> ../init.d/foobar
         /etc/rc3.d/S20foobar -> ../init.d/foobar
         /etc/rc4.d/S20foobar -> ../init.d/foobar
         /etc/rc5.d/S20foobar -> ../init.d/foobar

`update-rc.d` creará enlaces **K** (stop) en `rc0.d`, `rc1.d` y `rc6.d`, y enlaces **S** (start) en `rc2.d`, `rc3.d`, `rc4.d` y `rc5.d`.

Si no desea que un paquete instalado se inicie automáticamente, utilice `update-rc.d` para eliminar los enlaces de inicio, por ejemplo para deshabilitar el inicio de `dovecot` en el arranque:

        # update-rc.d -f dovecot remove
        Removing any system startup links for /etc/init.d/dovecot ...
         /etc/rc2.d/S24dovecot
         /etc/rc3.d/S24dovecot
         /etc/rc4.d/S24dovecot
         /etc/rc5.d/S24dovecot

La opción `-f` (force) es necesaria si el script `rc` aún existe. Si instala un paquete `dovecot` actualizado, los enlaces se restaurarán. Para evitarlo, cree enlaces de «stop» en los directorios del nivel de ejecución de inicio:

        # update-rc.d -f dovecot stop 24 2 3 4 5 .
        Adding system startup for /etc/init.d/dovecot ...
         /etc/rc2.d/K24dovecot -> ../init.d/dovecot
         /etc/rc3.d/K24dovecot -> ../init.d/dovecot
         /etc/rc4.d/K24dovecot -> ../init.d/dovecot
         /etc/rc5.d/K24dovecot -> ../init.d/dovecot

> Nota: No olvides el punto (.) al final del comando.

## Uso de los systemd targets

En lugar de niveles de ejecución predefinidos, `systemd` utiliza *targets* para definir el estado del sistema. Estos *targets* están representados por unidades. Las *unidades de target* terminan con las extensiones de fichero `.target` y su único propósito es agrupar otras unidades `systemd` a través de una cadena de dependencias. Esto también significa que, en comparación con los niveles de ejecución init, pueden estar activos varios targets al mismo tiempo.

Por ejemplo, la unidad `graphical.target`, inicia los servicios como el Gestor de pantalla de GNOME pero también depende del `multi-user.target` (que es el estado no gráfico del sistema) que a su vez depende del `basic.target`.

Antes de continuar con la gestión y el uso de los targets, debe saber qué directorios se utilizan para almacenar los ficheros de destino predeterminados y cómo puede sobrescribirlos.

Al igual que con todas las *unidades target*, los ficheros de destino predeterminados se almacenan en `/usr/lib/systemd`, los ficheros de este directorio son creados por el proveedor del software que ha instalado y nunca deben cambiarse, exceptuando los scripts de instalación. El directorio donde puede almacenar sus targets personalizados y sobrescribirlos es `/etc/systemd`, todo lo que se escriba en este directorio tiene precedencia sobre los ficheros en `/usr/lib/systemd`.

Hay varias formas de sobrescribir o añadir propiedades a los ficheros de la unidad. Puede crear un fichero completamente nuevo con el mismo nombre, por ejemplo `ssh.server`, en el directorio `/etc/systemd/system`. Esto sobrescribirá la definición completa de la unidad. Si sólo desea añadir o cambiar algunas propiedades, puede crear un nuevo directorio en `/etc/systemd/system` con el nombre de la unidad anadiendo `.d`, por ejemplo `sshd.server.d`. En este directorio puede crear ficheros con una extensión `.conf` en la que puede colocar las propiedades que desee añadir o anular. Ambas formas también se pueden hacer usando el comando `systemctl` (esto se describe con más detalle en el capítulo 206.3).

También hay una tercera ubicación disponible en la que puede colocar ficheros para redefinir las configuradiones de su unidad, este es el directorio `/run/systemd`. Las definiciones en este directorio tienen prioridad sobre los ficheros en `/usr/lib/systemd`, pero no sobre los de `/etc/systemd`. Las modificaciones en `/run/systemd` sólo se utilizarán hasta que se reinicie el sistema, ya que todos los ficheros en `/run/systemd` se eliminarán al reiniciar el sistema.

Para obtener una visión general de todas las redefiniciones activas, puede utilizar el comando `systemd-delta`. Este comando puede devolver los siguientes tipos:

* **masked** - Unidades enmascaradas, unidades que no pueden ser iniciadas.
* **equivalent** - Ficheros sustituidos que no difieren en su contenido.
* **redirected** - Enlaces simbólicos a otros ficheros de la unidad.
* **overridden** - Ficheros de unidad anulados.
* **extended** - Ficheros de unidad extendidos usando un fichero `.conf`.

También puede filtrar por los tipos anteriores usando la `-t` de `--type= flags`, estos toman una lista de los tipos anteriores. Si también desea ver ficheros sin modificar, puede añadir `unchanged` (sin modificar) como un tipo. Otras opciones que puede usar son `--diff=false` si no desea que `systemd-delta` muestre las diferencias de los ficheros sobreescritos, y `--no-pager` si no desea que la salida se canalice a un paginador.

Ahora que sabemos dónde están almacenados los ficheros de la unidad y cómo podemos sobrescribirlos, podemos echar un vistazo a los estados cambiantes del sistema.

Para obtener y cambiar el estado (predeterminado) del sistema utilizamos el comando `systemctl`. Una explicación mas completa se encuentra en el capítulo 206.3.

Si queremos obtener el *target* por defecto podemos ejecutar el siguiente comando:

	$ systemctl get-default

Esto dará como resultado el objetivo predeterminado actual que se utiliza al iniciar el sistema. Para obtener una lista de todas las unidades de destino actualmente cargadas, puede ejecutar el siguiente comando:

	$ systemctl list-units --type=target

Esto le dará una lista de todos los *targets* activos actualmente. Para obtener todas las *unidades de target* disponibles puedes ejecutar el siguiente comando:

	$ systemctl list-unit-files --type=target

Si desea cambiar el *target* por defecto puede utilizar el comando `systemctl set-default`. Por ejemplo, para establecer el *target* por defecto en `multi-user.target` puede ejecutar el siguiente comando:

	$ systemctl set-default multi-user.target

Este comando creará un enlace simbólico `/etc/systemd/system/default.target` que enlaza con el fichero de destino en `/usr/lib/systemd/system`.

Una comparación entre los *runlevels* de `init` y los *targets* del sistema:

| Runlevel | Target | Descripcion |
| -- | -- | -- |
| 0 | runlevel0.target, poweroff.target | Apagar y apagar el sistema |
| 1 | runlevel1.target, rescue.target | Establecer un shell de rescate (root-no red) |
| 2 | runlevel2.target, multi-user.target | Establecer un sistema multiusuario no gráfico. |
| 3 | runlevel3.target, multi-user.target | Establecer un sistema multiusuario no gráfico. |
| 4 | runlevel4.target, multi-user.target | Establecer un sistema multiusuario no gráfico. |
| 5 | runlevel5.target, graphical.target | Establecer un sistema multiusuario gráfico. |
| 6 | runlevel6.target, reboot.target | Apagar y reinicar el sistema. |

También puede cambiar el estado del sistema en *runtime* (tiempo de ejecución), esto se puede hacer usando el comando `systemctl isolate`. Esto detendrá todos los servicios no definidos para el *target* seleccionado e iniciará todos los servicios definidos para el nuevo *target*.

Por ejemplo, para ir al modo de rescate, puede ejecutar el siguiente comando:

	$ systemctl isolate rescue.target

Esto detendrá todos los servicios excepto los definidos para el *target de rescate*. Tenga en cuenta que este comando no notificará a ningún usuario conectado de la acción.

También hay algunos atajos disponibles para cambiar el estado del sistema, estos tienen un comportamiento adicional al notificar a los usuarios registrados de la acción. Por ejemplo, otra forma de situar el sistema en modo de rescate es la siguiente:

	$ systemctl rescue

Esto primero notificará a los usuarios registrados de la acción y luego detendrá todos los servicios no definidos en el *target*. Si no desea que los usuarios sean notificados, puede añadir el indicador `--no-wall`.

Si su sistema está demasiado dañado para utilizar el modo de rescate, también hay un modo de emergencia disponible. Esto se puede iniciar usando el siguiente comando:

	$ systemctl emergency

Si desea iniciar ciertos servicios en el arranque, debe habilitarlos. Puede habilitar servicios utilizando el comando `systemctl enable`. Por ejemplo, para habilitar `sshd`, ejecute el siguiente comando:

	# systemctl enable sshd.service

Para desactivar el servicio de nuevo, utilice el comando `systemctl disable`. Por ejemplo:

	# systemctl disable sshd.service

El fichero de unidad del servicio define en qué estado del sistema se ejecutará el servicio. Esto se configura a través de la opción `WantedBy=` en la sección`[Install]`. Si está configurado en `multiuser.target`, se ejecutará tanto en modo gráfico como no gráfico.

## El estándar LSB

La *base estándar de Linux* (LSB) define una interfaz para programas de aplicación que se compilan y empaquetan para implementaciones que cumplen con LSB. Por lo tanto, un programa compilado en un entorno compatible con LSB se ejecutará en cualquier distribución que soporte el estándar LSB. Los programas compatibles con LSB pueden confiar en la disponibilidad de ciertas bibliotecas estándar. El estándar también incluye una lista de utilidades y scripts obligatorios que definen un entorno adecuado para la instalación de binarios compatibles con LSB.

La especificación incluye información detallada de la arquitectura del procesador. Esto implica que el LSB es una familia de especificaciones, más que una sola. En otras palabras: si su binario compatible con LSB fue compilado para un sistema basado en Intel, no se ejecutará, por ejemplo, en un sistema compatible con LSB basado en Alpha, sino que se instalará y ejecutará en cualquier sistema compatible con LSB basado en Intel. Por lo tanto, las especificaciones LSB consisten en una parte común y otra específica de la arquitectura; «LSB-generic» o «generic LSB» y «LSB-arch» o «archLSB».

El estándar LSB enumera las bibliotecas genéricas que deberían estar disponibles, por ejemplo, `libdl`, `libcrypt`, `libpthread`, etc., y proporciona una lista de bibliotecas específicas del procesador, como `libc` y `libm`. El estándar también enumera las rutas de búsqueda de estas bibliotecas, sus nombres y formato (ELF). Otra sección se ocupa de la forma en que se debe implementar el enlace dinámico. Para cada biblioteca estándar se proporciona una lista de funciones y se enumeran las definiciones de datos y los ficheros de cabecera correspondientes.

El LSB define una lista de más de 130 comandos que deberían estar disponibles en un sistema compatible con LSB, así como sus convenciones y comportamiento de llamada. Algunos ejemplos son `cp`, `tar`, `kill` y `gzip`, y los lenguajes de ejecución `perl` y `python`.

El comportamiento esperado de un sistema compatible con LSB durante la inicialización del sistema forma parte de la especificación LSB. También lo es una definición del sistema `cron`, y son acciones, funciones y ubicación de los scripts de inicio. Cualquier script de inicio compatible con LSB debe ser capaz de manejar las siguientes opciones: `start`, `stop`, `restart`, `force-reload` y `status`. Las opciones de `reload` e intento de `restart` son opcionales. El estándar también enumera las definiciones de *runlevels* y listados de nombres de usuarios y grupos, y sus correspondientes UID's/GID's.

Aunque es posible instalar un programa compatible con LSB sin el uso de un gestor de paquetes (aplicando un script que contenga sólo comandos compatibles con LSB), la especificación LSB contiene una descripción de los paquetes de software y sus convenciones de nomenclatura.

> Nota: LSB utiliza el estándar *Red Hat Package Manager*. Las distribuciones compatibles con LSB basadas en Debian pueden leer paquetes RPM usando el comando `alien`.

Los estándares LSB se refieren frecuentemente a otros estándares bien conocidos, por ejemplo *ISO/IEC 9945-2009* (Portable OS base, muy parecido a Unix). Cualquier implementación que se ajuste a la LSB debe proporcionar las partes obligatorias de la jerarquía del sistema de ficheros tal y como se especifica en el *Filesystem Hierarchy Standard* (Estándar de Jerarquía del Sistema de Ficheros) **FHS**, así como una serie de requisitos específicos de la LSB. Véase también la sección sobre la norma FHS.

### El entorno bootscript y comandos

Inicialmente, Linux contenía sólo un conjunto limitado de servicios y tenía un entorno de arranque muy simple. A medida que Linux iba madurando y el número de servicios en una distribución creció, el número de *initscripts* creció en consecuencia. Después de un tiempo surgió un conjunto de estándares. Los *initscripts* incluían habitualmente algún otro script, que contenía funciones para iniciar, detener y verificar un proceso.

El estándar LSB enumera una serie de funciones que deberían estar disponibles para los scripts de nivel de ejecución. Estas funciones deberían estar listadas en ficheros en el directorio `/lib/lsb/init-functions` y necesitan implementar (al menos) las siguientes funciones:

1. `start_daemon [-f] [-n nicelevel] [-p pidfile] pathname [args...]` - Ejecuta el programa especificado como un demonio. La función `start_daemon` comprobará si el programa ya se está ejecutando. Si es así, no iniciará otra copia del demonio a menos que se indique la opción `-f`. La opción `-n` especifica un nivel *nice*.
2. `killproc [-ppidfile] pathname [signal]` - Detendrá el programa especificado, tratando de terminarlo usando primero la señal indicada. Si eso falla, se enviará la señal **SIGTERM**. Si un programa ha sido finalizado, el *pidfile* debería ser eliminado si el proceso no lo ha hecho ya.
3. `pidofproc [-p pidfile] pathname` - Devuelve uno o más identificadores de proceso para un demonio particular, como se especifica en la ruta. Los múltiples identificadores de proceso están separados por un solo espacio.

En algunos casos, estas funciones se proporcionan como comandos independientes y los scripts simplemente aseguran que la ruta a estos scripts se establece correctamente. A menudo también se incluyen algunas funciones de registro y de visualización de las líneas de estado.

## Cambio y configuración de los runlevels

El cambio de *runlevels* en una máquina en marcha requiere la comparación de los servicios que se ejecutan en el *runlevel* actual con aquellos que deben ejecutarse en el nuevo *runlevel*. Posteriormente, es probable que algunos procesos necesiten ser detenidos y otros deben ser iniciados.

Recordemos que los *initscripts* para un nivel de ejecución «X» están agrupados en el directorio `/etc/rc.d/rcX.d` (o, en sistemas más nuevos (basados en LSB), en `/etc/init.d/rcX.d`). Los nombres de fichero determinan cómo se llaman los scripts: si el nombre comienza con una «K», el script se ejecutará con la opción de parada, si el nombre comienza con una «S», el script se ejecutará con la opción de iniciar. El procedimiento normal durante un cambio de *runlevel* es detener los procesos superfluos primero y luego iniciar los nuevos.

Los scripts de inicio se encuentran en `/etc/init.d`. Los ficheros que se encuentran en el directorio `rcX.d` son enlaces simbólicos que enlazan con ellos. En muchos casos, los scripts de `start` y de `stop` son enlaces simbólicos al mismo script. Esto implica que tales scripts de inicio deben ser capaces de manejar al menos las opciones de inicio y parada.

Por ejemplo, el enlace simbólico llamado `S06syslog` en `/etc/init.d/rc3.d` podría apuntar al script `/etc/init.d/syslog`, al igual que el enlace simbólico encontrado en `/etc/init.d/rc2.d`, llamado `K17syslog`.

El orden en el que se detienen o se inician los servicios puede ser de gran importancia. Algunos servicios pueden iniciarse simultáneamente, otros deben iniciarse en un orden estricto. Por ejemplo, su red debe estar en funcionamiento antes de que pueda iniciar el `httpd`. El orden se determina por los nombres de los enlaces simbólicos. Las convenciones de nomenclatura dictan que los nombres de los scripts de inicio (los que se encuentran en los directorios `rcN.d`) incluyan dos dígitos, justo después de la letra inicial. Se ejecutan en orden alfabético.

En los comienzos, los administradores del sistema creaban estos enlaces a mano. Más tarde, la mayoría de los distribuidores de Linux decidieron proporcionar comandos/scripts que permiten al administrador habilitar/deshabilitar ciertos scripts en ciertos *runlevels* y comprobar qué sistemas (comandos) se iniciarían en cada *runlevel*. Estos comandos normalmente gestionarán los dos enlaces mencionados y los nombrarán de tal forma que los scripts se ejecuten en el orden correcto.

## El comando chkconfig

Otra herramienta para gestionar la correcta vinculación de los scripts de inicio (init) es `chckconfig`. En algunos sistemas (por ejemplo, SuSE/Novell) sirve como front-end para `insserv` y utiliza el bloque de comentarios estandarizado LSB para mantener su administración. En sistemas más antiguos mantiene su propia sección especial de comentarios, que tiene una sintaxis mucho más simple y menos flexible. Esta antigua sintaxis consta de dos líneas, una de ellas es una descripción del servicio, comienza con la descripción de la palabra clave. La otra línea comienza con la palabra clave `chkconfig`:, y enumera los niveles de ejecución para los que se debe iniciar el servicio y la prioridad (que determina en qué orden se ejecutarán los scripts mientras se cambian los niveles de ejecución). Por ejemplo:

        # Init script for foo daemon
        #
        # description: food, the foo daemon
        # chkconfig: 2345 55 25
        #
        #

Esto denota que el daemon `foo` se iniciará en los niveles de ejecución 2, 3, 4 y 5, tendrá prioridad 55 en la cola de los *initscripts* que se ejecutan durante el inicio y prioridad 25 en la cola de los *initscripts* que se ejecutan si el daemon tiene que ser detenido.

La utilidad `chkconfig` se puede utilizar para visualizar los servicios quw se iniciarán y en qué *runlevels*, para añadir un servicio o para eliminarlo de un *runlevel*, y para añadir un servicio completo o eliminarlo de los scripts de inicio.

> Nota: Estamos dando algunos ejemplos aquí, pero ten en cuenta que hay varias versiones de `chkconfig` disponibles. Por favor, consulta primero las *manpages* para el comando `chkconfig` en tu distribución.

`chkconfig` no deshabilita o habilita automáticamente un servicio de forma inmediata, sino que simplemente cambia los enlaces simbólicos. Si el daemon `cron` se está ejecutando y estamos en un sistema basado en Red Hat que se está ejecutando en el runlevel 2, el comando:

        # chkconfig --levels 2345 crond off

cambiaría la administración borrando los enlaces simbolicos para un servicio gestionado, pero no detendría el daemon `cron` inmediatamente. También tenga en cuenta que en un sistema Red Hat es posible especificar más de un *runlevel*, como hicimos en nuestro ejemplo anterior. En los sistemas Novell/SuSE, puede utilizar:

        # chkconfig food 2345

y para cambiar esto para que sólo se ejecute en el nivel de ejecución 1, simplemente use:

        # chkconfig food 1
        # chkconfig --list

mostrará el estado actual de los servicios y los *runlevels* en los que están activos. Por ejemplo, las siguientes dos líneas pueden ser parte de la salida:

	xdm			0:off   1:off   2:off   3:off   4:off   5:on    6:off
	xfs     		0:off   1:off   2:off   3:off   4:off   5:off   6:off

Indican que el servicio `xfs` no se inicia en ningún nivel de ejecución y que el servicio `xdm` sólo se iniciará al cambiar al nivel de ejecución 5.

Para añadir un nuevo servicio, digamos el daemon `foo`, creamos un nuevo *script init* y le ponemos el nombre del servicio, en este caso podríamos usar `food`. Este script se pone consecutivamente en el directorio `/etc/init.d`, después de lo cual necesitamos insertar el encabezado apropiado en ese script (ya sea el antiguo encabezado de `chkconfig`, o el nuevo encabezado compatible con LSB) y luego ejecutar:

        # chkconfig --add food

Para eliminar el servicio `foo` de todos los *runlevels*, puede escribir:

        # chkconfig --del food

> Nota: Tenga en cuenta que el script de `food` permanecerá en el directorio `/etc/init.d/`.
