# 205.1 - Configuración básica de la red

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contenidos

- [Configuración de la interfaz de red](#configuración-de-la-interfaz-de-red)
- [La interfaz loopback](#la-interfaz-loopback)
- [Interfaces Ethernet](#interfaces-ethernet)
- [Routing (Enrutamiento) a través de un Gateway](#routing-enrutamiento-a-través-de-un-gateway)
- [El comando ip](#el-comando-ip)
- [ARP, Address Resolution Protocol (Protocolo de resolución de direcciones)](#arp-address-resolution-protocol-protocolo-de-resolución-de-direcciones)
- [Redes inalámbricas](#redes-inalámbricas)
    - [iw](#iw)
    - [iwconfig](#iwconfig)
    - [iwlist](#iwlist)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Configuración de la interfaz de red

La «mayoría» de los dispositivos de red son compatibles con los kernels modernos. Sin embargo, deberá configurar sus dispositivos para que se integren en su red. Necesitarán una dirección IP (IPv4, IPv6 o ambas), es posible que haya que dar a conocer varios *gateways* o *routers* para permitir el acceso a otras redes y establecer la ruta por defecto. Estas tareas se realizan normalmente desde el script de inicialización de red cada vez que se arranca el sistema. Las herramientas básicas para este proceso son `ifconfig` (donde "if" significa interfaz) y `route`.

El comando `ifconfig` sigue siendo muy utilizado. Configura una interfaz y la hace accesible a la capa de red del kernel. Se pueden configurar una dirección IP, una subred, una dirección de difusión y otros parámetros. La herramienta también puede utilizarse para activar y desactivar la interfaz, también conocida como `up` y `down` de la interfaz. Una interfaz activa enviará y recibirá datagramas IP a través de la interfaz. La forma más sencilla de invocarlo es:

	# ifconfig interface ip-address
			
Este comando asigna una dirección IP a la interfaz y la activa. Todos los demás parámetros se ajustan a los valores predeterminados. Por ejemplo, la máscara de red predeterminada se deriva de la clase de red de la dirección IP, como 255.255.0.0.0 para una dirección de clase B.

El comando `route` le permite añadir o eliminar rutas de la tabla de enrutamiento del kernel. Se puede ejecutar como:

	# route {add|del}[-net|-host] target[if]
			
Los argumentos `add` y `del` determinan si se debe añadir o eliminar la ruta al destino. Los argumentos `-net` y `-host` indican al comando de ruta si el objetivo es una red o un host (se asume un host si no se especifica). El argumento `if` especifica la interfaz y es opcional, y le permite especificar a qué interfaz de red debe dirigirse la ruta - el kernel de Linux hace una conjetura sensata si usted no proporciona esta información.

## La interfaz loopback

Las implementaciones TCP/IP incluyen una interfaz de red virtual que puede utilizarse para emular el tráfico de red entre dos procesos en el mismo host. La interfaz *loopback* no está conectada a ninguna red real, sino que se implementa completamente dentro del software de red del sistema operativo. El tráfico enviado a la dirección IP de *loopback* (a menudo se utiliza la dirección 127.0.0.1) se devuelve simplemente a la pila de software de red como si se hubiera recibido desde otro dispositivo. La dirección IPv6 utilizada es ::1, y comúnmente el nombre «localhost» se utiliza como `hostname`. Es la primera interfaz que se activa durante el arranque:

	# ifconfig lo 127.0.0.0.1
			
Ocasionalmente, verá el *hostname* ficticio de localhost que se está utilizando en lugar de la dirección IP. Esto requiere una configuración adecuada del fichero `/etc/hosts`:

	# Ejemplo de entrada de /etc/hosts para localhost
	127.0.0.0.1 localhost
			
Para ver la configuración de una interfaz, simplemente ejecute `ifconfig` con el nombre de la interfaz como único argumento:

	$ ifconfig lo
	lo        Link encap:Local Loopback inet addr:127.0.0.1
	Mask:255.0.0.0 UP LOOPBACK RUNNING MTU:3924 Metric:1 RX packets:0
	errors:0 dropped:0 overruns:0 frame:0 TX packets:0 errors:0 dropped:0
	overruns:0 carrier:0 Collisions:0

Este ejemplo muestra que a la interfaz loopback se le ha asignado por defecto una máscara de red de 255.0.0.0. 127.0.0.1 es una dirección de clase A.

Estos pasos son suficientes para utilizar aplicaciones de red en un host independiente. Después de añadir estas líneas a su script de inicialización de red (se suele proporcionar desde la distribución) y asegurar su ejecución en el momento del arranque reiniciando su máquina, puede probar la interfaz *loopback*. Por ejemplo, `telnet localhost` debería establecer una conexión `telnet` a su host, mostrando un indicador de `login`:

La interfaz *loopback* se utiliza a menudo como banco de pruebas durante el desarrollo, pero existen otras aplicaciones. Por ejemplo, todas las aplicaciones basadas en RPC utilizan la interfaz *loopback* para registrarse con el daemon `portmapper` al inicio. Estas aplicaciones incluyen NIS y NFS. Por lo tanto, la interfaz loopback siempre debe estar configurada, tanto si el equipo está conectado a una red como si no.

## Interfaces Ethernet

La configuración de una interfaz Ethernet es prácticamente la misma que la interfaz *loopback:* sólo se necesitan unos pocos parámetros más cuando se utiliza subnetting.

Supongamos que hemos subdividido la red IP, que originalmente era una red de clase B, en subredes de clase C. Para que la interfaz reconozca esto, el uso de `ifconfig` se vería así:

	# ifconfig eth0 172.16.1.2 netmask 255.255.255.0

Este comando asigna a la interfaz `eth0` una dirección IP de 172.16.1.2. Si hubiéramos omitido la máscara de red, `ifconfig` deduciría la máscara de red de la clase de red IP, lo que resultaría en una máscara de red incorrecta de 255.255.0.0. Ahora se muestra una comprobación rápida:

	# ifconfig eth0
	eth0      Link encap 10Mps Ethernet HWaddr
	          00:00:C0:90:B3:42 inet addr 172.16.1.2 Bcast 172.16.1.255 Mask
	          255.255.255.0 UP BROADCAST RUNNING MTU 1500 Metric 1 RX packets 0
	          errors 0 dropped 0 overrun 0 TX packets 0 errors 0 dropped 0 overrun 0
			
Puede ver que `ifconfig` configura automáticamente la dirección *broadcast* (el campo Bcast) con el valor habitual, que es el número de red del host con todos los bits del host configurados. Además, la unidad de transmisión máxima (el tamaño máximo de los datagramas IP que el kernel generará para esta interfaz) se ha establecido en el tamaño máximo de los paquetes Ethernet: 1.500 bytes. Los valores predeterminados suelen ser los que se utilizarán, pero todos estos valores se pueden sustituir si es necesario.

## Routing (Enrutamiento) a través de un Gateway

No necesita enrutado si su host está en una sola Ethernet. Sin embargo, con mucha frecuencia, las redes están conectadas entre sí mediante *gateways.* Estos gateways pueden simplemente enlazar dos o más Ethernet, pero también pueden proporcionar un enlace con el mundo exterior, como Internet. Para utilizar un *gateway,* debe proporcionar información de enrutamiento adicional a la capa de red.

Imagine dos ethernets conectados a través de un gateway, el host *romeo.* Asumiendo que *romeo* ya ha sido configurado, sólo tenemos que añadir una entrada a la tabla de enrutamiento que le dice al kernel que todos los hosts en la otra red pueden ser alcanzados a través de *romeo.* La invocación apropiada de la ruta se muestra a continuación; la palabra clave `gw` le dice que el siguiente argumento denota una puerta de enlace (gateway):

	# route add -net 172.16.0.0 netmask 255.255.255.255.0 gw romeo

Por supuesto, cualquier host de la otra red con el que desee comunicarse debe tener una entrada de enrutamiento para nuestra red. De lo contrario, sólo podrá enviar datos a la otra red, pero los hosts de la otra red no podrán responder.

Este ejemplo sólo describe un *gateway* que conmuta paquetes entre dos ethernets aislados. Ahora asuma que *romeo* también tiene una conexión a Internet (digamos, a través de un enlace PPP adicional). En este caso, queremos que los datagramas de cualquier red de destino sean entregados a *romeo*. Esto se puede lograr convirtiéndolo en la puerta de enlace predeterminada:

	# route add default gw romeo
			
Algo que a menudo se malinterpreta es que sólo se puede configurar UNA puerta de enlace predeterminada. Puede que tenga muchas puertas de enlace, pero sólo una puede ser la predeterminada.

El nombre de red predeterminado es la abreviatura de 0.0.0.0, que denota la ruta predeterminada. La ruta predeterminada coincide con cada destino y se utilizará si no hay una ruta más específica disponible.

## El comando ip

En los últimos años mucha gente ha defendido el uso del nuevo comando ´/sbin/ip´. También se puede utilizar para mostrar o manipular dispositivos de enrutamiento y de red, y también se puede utilizar para configurar o mostrar enrutamiento de políticas y túneles. Sin embargo, las herramientas antiguas `ifconfig` y `route` también se pueden utilizar si es más conveniente. Un caso de uso para el comando `ip` sería mostrar las direcciones IP utilizadas en las interfaces de red de una manera más concisa en comparación con `ifconfig`:

	# ip addr show
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 16436 qdisc noqueue state UNKNOWN 
	    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
	    inet 127.0.0.1/8 scope host lo
	    inet6 ::1/128 scope host 
	       valid_lft forever preferred_lft forever
	2: p8p1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP qlen 1000
	    link/ether 00:90:f5:b6:91:d1 brd ff:ff:ff:ff:ff:ff
	    inet 192.168.123.181/24 brd 192.168.123.255 scope global p8p1
	    inet6 fe80::290:f5ff:feb6:91d1/64 scope link 
	       valid_lft forever preferred_lft forever
	3: wlan0: <BROADCAST,MULTICAST> mtu 1500 qdisc mq state DOWN qlen 1000
	    link/ether 88:53:2e:02:df:14 brd ff:ff:ff:ff:ff:ff
	4: vboxnet0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN qlen 1000
	    link/ether 0a:00:27:00:00:00 brd ff:ff:ff:ff:ff:ff

a diferencia de ifconfig:

	# ifconfig -a
	lo        Link encap:Local Loopback  
	          inet addr:127.0.0.1  Mask:255.0.0.0
	          inet6 addr: ::1/128 Scope:Host
	          UP LOOPBACK RUNNING  MTU:16436  Metric:1
	          RX packets:48 errors:0 dropped:0 overruns:0 frame:0
	          TX packets:48 errors:0 dropped:0 overruns:0 carrier:0
	          collisions:0 txqueuelen:0 
	          RX bytes:4304 (4.2 KiB)  TX bytes:4304 (4.2 KiB)

	p8p1      Link encap:Ethernet  HWaddr 00:90:F5:B6:91:D1  
	          inet addr:192.168.123.181  Bcast:192.168.123.255  Mask:255.255.255.0
	          inet6 addr: fe80::290:f5ff:feb6:91d1/64 Scope:Link
	          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
	          RX packets:6653 errors:0 dropped:0 overruns:0 frame:0
	          TX packets:7609 errors:0 dropped:0 overruns:0 carrier:0
	          collisions:0 txqueuelen:1000 
	          RX bytes:3843849 (3.6 MiB)  TX bytes:938833 (916.8 KiB)
	          Interrupt:53 

	vboxnet0  Link encap:Ethernet  HWaddr 0A:00:27:00:00:00  
	          BROADCAST MULTICAST  MTU:1500  Metric:1
	          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
	          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
	          collisions:0 txqueuelen:1000 
	          RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)

	wlan0     Link encap:Ethernet  HWaddr 88:53:2E:02:DF:14  
	          BROADCAST MULTICAST  MTU:1500  Metric:1
	          RX packets:2 errors:0 dropped:0 overruns:0 frame:0
	          TX packets:11 errors:0 dropped:0 overruns:0 carrier:0
	          collisions:0 txqueuelen:1000 
	          RX bytes:330 (330.0 b)  TX bytes:1962 (1.9 KiB)

Un ejemplo de uso de `ip` como alternativa a `ifconfig` cuando se configura una interfaz de red (`p8p1`):

	# ifconfig p8p1 192.168.123.15 netmask 255.255.255.255.0 broadcast 192.168.123.255
			
puede ser reemplazado por:

	# ip addr add 192.168.123.15/24 broadcast 192.168.123.255 dev p8p1
			
`ip` también puede ser usada como alternativa para el comando `route`:

	# ip route add 192.168.123.254.254/24 dev p8p1
			
	# ip route show
	192.168.123.0/24 dev p8p1  proto kernel  scope link  src 192.168.123.181  metric 1 
	default via 192.168.123.254 dev p8p1  proto static 
			
La salida del comando `route` en este caso sería:

	# route
	Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
	192.168.123.0   *               255.255.255.0   U     1      0        0 p8p1
	default         192.168.123.254 0.0.0.0         UG    0      0        0 p8p1
			
Como otro ejemplo, para añadir una ruta estática a la red 192.168.1.0 sobre `eth0`, use:

	# ip route add 192.168.1.0/24 dev eth0
			
Para más información, por favor lea las páginas de man de `ip`.

## ARP, Address Resolution Protocol (Protocolo de resolución de direcciones)

En el modelo de red ISO hay siete capas. El Protocolo de Internet es un protocolo de capa 3 y el NIC es un dispositivo de capa 2. En una red local (capa 2) los dispositivos se conocen entre sí por la dirección MAC (Media Access Control). En la red IP (capa 3) los dispositivos se conocen por su dirección IP.

Para permitir la transferencia de datos desde y hacia la capa 3, la comunicación IP requiere un protocolo para mapear entre la capa 2 y la capa 3. Este protocolo se conoce como ARP (Address Resolution Protocol). ARP crea un mapeo entre una dirección IP y la dirección MAC donde la dirección IP está configurada.

Cuando los dispositivos habilitados para IP desean comunicarse, el kernel del dispositivo de origen entrega el paquete IP al software del driver de la interfaz de red y solicita que se entregue el paquete IP al destinatario. La única forma de comunicarse en una Ethernet local es por medio de una dirección MAC, las direcciones IP no sirven para nada allí. Para averiguar la dirección MAC del destinatario con la dirección IP correcta, el driver de red de la interfaz en el lado del origen enviará una solicitud ARP. Una petición ARP es una petición de broadcast (difusión): se envía a cualquier ordenador de la red local. El ordenador que tenga la dirección IP solicitada responderá con su dirección MAC. El remitente dispone entonces de toda la información necesaria para transmitir el paquete. Además, las direcciones MAC e IP se almacenan en una caché local para referencia futura.

El comando `arp` puede usarse para mostrar la caché ARP. Ejemplo:

	# arp
	Address                  HWtype  HWaddress           Flags Mask            Iface
	10.9.8.126               ether   	00:19:bb:2e:df:73   	C                     wlan0
			
La caché puede ser manipulada manualmente, por ejemplo, si un host es destruido, es posible que desee eliminar su entrada `arp`. Normalmente no es necesario que se moleste, ya que la caché no es demasiado persistente. Vea las páginas de man para más información sobre el comando `arp`.

> Nota: Además, existe el protocolo inverso ARP (RARP). Este protocolo se utiliza para permitir a un dispositivo Ethernet (capa 2) las direcciones IP que ha configurado. ARP: emisión IP y recepción MAC. RARP: difunde MAC y recibe IP.

## Redes inalámbricas

### iw

`iw` se utiliza para configurar dispositivos inalámbricos. Sólo admite el estándar nl80211 (netlink). Así que si `iw` no ve su dispositivo, esta podría ser la razón. Debe usar `iwconfig` (desde el paquete `wireless_tools)` e `iwlist` para configurar el dispositivo inalámbrico. Estos están usando el estándar WEXT. `wireless_tools` esta obsoleto, pero todavía ampliamente soportado.

Sintaxis:

	# iw command
	# iw [options] [object] command
			
Algunas opciones comunes:

* **dev** - Este es un objeto y el nombre del dispositivo inalámbrico debe seguir después de esta opción. Con el comando `iw dev` puedes ver el nombre de su dispositivo.
* **link** - Este es un comando y obtiene el estado de enlace de su dispositivo inalámbrico.
* **scan** - Se trata de un comando que analiza la red en busca de puntos de acceso disponibles.
* **connect** - Este es un comando que le permite conectarse a un punto de acceso (essid), puede especificar un canal detrás de él y/o su contraseña.
* **set** - Este es un comando que le permite establecer una interfaz/modo diferente. Por ejemplo, `ibss` si desea establecer el modo de operación en Ad-Hoc. O establezca el estado de ahorro de energía de la interfaz.

Ejemplos:

	# iw dev wlan0 link 				
	# iw dev wlan0 scan 				
	# iw dev wlan0 connect "Access Point" 		
	# iw dev wlan0 connect "Access Point" 2432 		
	# iw dev wlan0 connect "Access Point"  0:"Your Key"
	# iw dev wlan0 set type ibss 			
	# iw dev wlan0 ibss join "Access Point" 2432
	# iw dev wlan0 ibss leave
	# iw dev wlan0 set power_save on
				
### iwconfig

`iwconfig` es similar a `ifconfig`, pero está dedicado a las interfaces inalámbricas. Se utiliza para ajustar los parámetros de la interfaz de red que son específicos de la operación inalámbrica (por ejemplo: la frecuencia). `iwconfig` también se puede utilizar para mostrar esos parámetros, y las estadísticas inalámbricas.

Todos los parámetros y estadísticas dependen del dispositivo. Cada driver proporcionará sólo algunos de ellos dependiendo del soporte de hardware, y el rango de valores que puede cambiar. Por favor, consulte la página de man de cada dispositivo para más detalles.

Sintaxis:

	# iwconfig [interface]
	# iwconfig interface [options]
				
Algunas opciones comunes:

* **essid** - Establezca el ESSID (o Nombre de Red - en algunos productos también puede ser llamado ID de Dominio). Con algunas tarjetas, puede desactivar la comprobación ESSID (ESSID promiscuo) con `off` o cualquiera (y luego volver a activarla). Si el ESSID de su red es una de las palabras clave especiales (apagado, encendido o cualquiera), debe usar `--` para escapar de ella.
* **mode** - Configure el modo de funcionamiento del dispositivo, que depende de la topología de la red. El modo puede ser Ad-Hoc (la red está compuesta de una sola celda y no tiene un punto de acceso), Managed (el nodo se conecta a una red compuesta de múltiples puntos de acceso, con roaming), Master (el nodo es el maestro de sincronización o actúa como punto de acceso), Repeater (el nodo reenvía paquetes entre otros nodos inalámbricos), Secondary (el nodo actúa como maestro/repetidor de respaldo), Monitor (el nodo no está asociado con ninguna celda y monitorea de forma pasiva todos los paquetes de la frecuencia) o Auto.

Ejemplos:

	# iwconfig wlan0 essid any
	# iwconfig wlan0 essid "Access Point"
	# iwconfig wlan0 essid -- "any"
	# iwconfig wlan0 mode Ad-Hoc
				
### iwlist

`iwlist` se utiliza para buscar redes inalámbricas disponibles y mostrar información adicional sobre ellas. La sintaxis es la siguiente:

	# iwlist [interface] command
				
`iwlist` puede mostrar ESSID's, información de frecuencia/canal, tasas de bits, tipo de encriptación, información de gestión de energía de otros nodos inalámbricos en el rango. La información que se muestra depende del hardware.

Algunas opciones útiles son:

* **scan** - Devuelve una lista de redes y puntos de acceso ad hoc. Dependiendo del tipo de tarjeta, se muestra más información, es decir, ESSID, intensidad de la señal, frecuencia. El escaneo sólo puede ser realizado por el usuario *root.* Cuando un usuario no *root* ejecuta el comando de análisis, los resultados del último análisis se devuelven, si están disponibles. Esto también puede lograrse añadiendo la última opción. Además, la opción `essid` puede utilizarse para buscar un ESSID específico. Dependiendo del driver, puede haber más opciones disponibles.
* **keys/enc[ryption]** - Enumera los tamaños de clave de cifrado compatibles y todas las claves de cifrado configuradas en el dispositivo.
